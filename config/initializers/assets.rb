# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "fonts")
Rails.application.config.assets.paths << Rails.root.join("vendor", "assets", "javascripts")

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# vendor.js, application.scss, and all non-JS/CSS in app/assets folder are already added.

Rails.application.config.assets.precompile += %w( application.js application.scss )
Rails.application.config.assets.precompile += %w( landing.js landing.scss)
