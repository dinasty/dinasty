Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'devise_overriden/registration',
    sessions: 'devise_overriden/session'
  }

  devise_scope :user do
    authenticated :user do
      root 'administration/conditional_appointments#index', as: :root
    end

    unauthenticated do
      root 'landing#index', as: :unauthenticated_root
    end
  end

  get 'article/:id', to: 'administration/articles#show', as: 'show_article'
  get 'conditional_appointments/new',
      to: 'administration/conditional_appointments#new',
      as: 'new_conditional_appointment'

  scope 'admin', module: :administration do
    resources :users, only: [:new, :create]
    resources :patients, except: [:new, :edit] do
      get 'change_status', on: :member

      resources :personal_cards, only: [:index]
    end

    resources :doctors, except: [:new, :edit] do
      get 'manage_status', on: :member
      get 'determine_doctor_time', on: :collection
    end

    resources :appointments, except: :new do
      get 'change_status', on: :member
    end

    resources :conditional_appointments, except: :new do
      get 'change_status', on: :member
    end

    resources :price_list_categories, only: [:index, :create, :update, :destroy]
    resources :age_types, except: [:new, :edit, :show]
    resources :videos, except: :show
    resources :articles, except: :show
    resources :procedures, except: [:new, :edit, :show]

    resources :personal_cards, only: [:edit, :update] do
      get 'history/:id', on: :collection,
                         to: 'personal_cards#history', as: 'history'
    end

    resources :treatments do
      get 'add_primary_fields', on: :collection
      get 'invoice/:treatment_id', on: :collection, to: 'treatments#invoice', as: 'invoice'
      get 'details', on: :member
      get 'manage_state', on: :member
      patch 'manage_treatment_statuses', on: :member
    end

    get 'by_types', to: 'searches#by_types'

    resources :feedbacks, only: [:index, :create] do
      get 'update_status/:id', on: :collection, to: 'feedbacks#update_status', as: :update_status
    end

    resources :calendar, only: :index do
      get 'events', on: :collection
    end
    resources :messages do
      post 'delivery', on: :collection
    end
  end
end
