set :output, "#{path}/log/cron.log"

every :day, :at => '9:00am'  do
  rake "sms:send"
end
every :day, :at => '8:00pm'  do
  rake "sms:send"
end
