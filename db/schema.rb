# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161226141657) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "age_types", force: :cascade do |t|
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "title"
    t.integer  "lower_age"
    t.integer  "upper_age"
    t.string   "constant_name"
  end

  create_table "appointments", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.datetime "appointment_date"
    t.integer  "status",           default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "start_time"
    t.datetime "end_time"
  end

  add_index "appointments", ["doctor_id"], name: "index_appointments_on_doctor_id", using: :btree
  add_index "appointments", ["patient_id"], name: "index_appointments_on_patient_id", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "doctor_id"
  end

  add_index "articles", ["doctor_id"], name: "index_articles_on_doctor_id", using: :btree

  create_table "conditional_appointments", force: :cascade do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "age"
    t.datetime "appointment_date"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "appointment_status", default: 0
    t.integer  "doctor_id"
    t.datetime "processed_time"
  end

  add_index "conditional_appointments", ["doctor_id"], name: "index_conditional_appointments_on_doctor_id", using: :btree

  create_table "deliveries", force: :cascade do |t|
    t.text     "message"
    t.string   "patient_ids", default: [],              array: true
    t.integer  "sms_id"
    t.integer  "status"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "phone"
    t.text     "work_history"
    t.integer  "work_experience"
    t.string   "position"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "work_status",     default: 0
    t.integer  "presence_state",  default: 0
  end

  add_index "doctors", ["user_id"], name: "index_doctors_on_user_id", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.string   "name"
    t.text     "message"
    t.string   "email"
    t.string   "phone"
    t.boolean  "show"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string   "phone"
    t.text     "text"
    t.datetime "send_time"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "treatment_id"
    t.string   "sms_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string   "phone"
    t.integer  "age"
    t.string   "gender"
    t.integer  "status",         default: 0
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "presence_state", default: 0
  end

  add_index "patients", ["user_id"], name: "index_patients_on_user_id", using: :btree

  create_table "personal_cards", force: :cascade do |t|
    t.integer  "patient_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "disease"
    t.string   "allergy"
    t.string   "medicine"
    t.string   "bleeding"
    t.boolean  "pregnancy"
    t.string   "gestational_age"
  end

  add_index "personal_cards", ["patient_id"], name: "index_personal_cards_on_patient_id", using: :btree

  create_table "price_list_categories", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "procedure_age_types", force: :cascade do |t|
    t.integer  "procedure_id"
    t.integer  "age_type_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.decimal  "price"
  end

  add_index "procedure_age_types", ["age_type_id"], name: "index_procedure_age_types_on_age_type_id", using: :btree
  add_index "procedure_age_types", ["procedure_id"], name: "index_procedure_age_types_on_procedure_id", using: :btree

  create_table "procedures", force: :cascade do |t|
    t.text     "title"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "category_id"
  end

  add_index "procedures", ["category_id"], name: "index_procedures_on_category_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "treatment_details", force: :cascade do |t|
    t.integer  "treatment_id"
    t.integer  "tooth"
    t.integer  "procedure_id"
    t.decimal  "price"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "treatment_details", ["procedure_id"], name: "index_treatment_details_on_procedure_id", using: :btree
  add_index "treatment_details", ["treatment_id"], name: "index_treatment_details_on_treatment_id", using: :btree

  create_table "treatments", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "doctor_id"
    t.date     "treatment_date"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "age_type_id"
    t.decimal  "price"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "state",                   default: 0
    t.string   "teeth",                   default: [],              array: true
    t.string   "procedures",              default: [],              array: true
    t.integer  "presence_state",          default: 0
    t.text     "complaint"
    t.text     "before_treatment_status"
    t.text     "treatment_process"
  end

  add_index "treatments", ["age_type_id"], name: "index_treatments_on_age_type_id", using: :btree
  add_index "treatments", ["doctor_id"], name: "index_treatments_on_doctor_id", using: :btree
  add_index "treatments", ["patient_id"], name: "index_treatments_on_patient_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "surname"
    t.integer  "user_type",              default: 0
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "avatar"
    t.string   "default_image"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "title"
    t.string   "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "appointments", "doctors"
  add_foreign_key "appointments", "patients"
  add_foreign_key "articles", "doctors"
  add_foreign_key "conditional_appointments", "doctors"
  add_foreign_key "doctors", "users"
  add_foreign_key "patients", "users"
  add_foreign_key "personal_cards", "patients"
  add_foreign_key "procedure_age_types", "age_types"
  add_foreign_key "procedure_age_types", "procedures"
  add_foreign_key "treatment_details", "procedures"
  add_foreign_key "treatment_details", "treatments"
  add_foreign_key "treatments", "age_types"
  add_foreign_key "treatments", "doctors"
  add_foreign_key "treatments", "patients"
end
