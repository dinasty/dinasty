class AddProcessedTimeToConditionalAppointments < ActiveRecord::Migration
  def change
    add_column :conditional_appointments, :processed_time, :datetime
  end
end
