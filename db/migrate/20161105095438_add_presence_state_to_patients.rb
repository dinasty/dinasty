class AddPresenceStateToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :presence_state, :integer, default: 0
  end
end
