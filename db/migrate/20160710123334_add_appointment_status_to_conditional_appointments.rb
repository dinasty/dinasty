class AddAppointmentStatusToConditionalAppointments < ActiveRecord::Migration
  def change
    add_column :conditional_appointments, :appointment_status, :integer, default: 0
  end
end
