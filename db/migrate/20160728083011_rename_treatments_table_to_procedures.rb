class RenameTreatmentsTableToProcedures < ActiveRecord::Migration
  def change
    rename_table :treatments, :procedures
  end
end
