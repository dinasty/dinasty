class AddColumnsToAgeTypesTable < ActiveRecord::Migration
  def change
    remove_column :age_types, :age_range, :integer
    add_column :age_types, :title, :string
    add_column :age_types, :lower_age, :integer
    add_column :age_types, :upper_age, :integer
  end
end
