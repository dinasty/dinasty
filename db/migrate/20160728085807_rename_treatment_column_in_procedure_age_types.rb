class RenameTreatmentColumnInProcedureAgeTypes < ActiveRecord::Migration
  def change
    rename_column :procedure_age_types, :treatment_id, :procedure_id
  end
end
