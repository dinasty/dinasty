class AddPresenceStateToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :presence_state, :integer, default: 0
  end
end
