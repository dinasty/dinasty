class AddSmsIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :sms_id, :string
  end
end
