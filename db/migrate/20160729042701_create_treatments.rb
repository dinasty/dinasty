class CreateTreatments < ActiveRecord::Migration
  def change
    create_table :treatments do |t|
      t.references :patient, index: true, foreign_key: true
      t.references :doctor, index: true, foreign_key: true
      t.references :procedure, index: true, foreign_key: true
      t.datetime :treatment_date
      t.datetime :start_time
      t.datetime :end_time
      t.references :age_type, index: true, foreign_key: true
      t.string :teeth_number
      t.decimal :price

      t.timestamps null: false
    end
  end
end
