class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.references :doctor, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.datetime :attendance_date
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
