class RemoveColumnsFromTreatmentsTable < ActiveRecord::Migration
  def change
    remove_column :treatments, :age_range, :integer
    remove_column :treatments, :price, :decimal
  end
end
