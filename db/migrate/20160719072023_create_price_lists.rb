class CreatePriceLists < ActiveRecord::Migration
  def change
    create_table :price_lists do |t|
      t.text :title
      t.integer :age_range
      t.decimal :price

      t.timestamps null: false
    end
  end
end
