class AddConstantNameToAgeTypes < ActiveRecord::Migration
  def change
    add_column :age_types, :constant_name, :string
  end
end
