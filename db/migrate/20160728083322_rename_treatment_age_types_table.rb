class RenameTreatmentAgeTypesTable < ActiveRecord::Migration
  def change
    rename_table :treatment_age_types, :procedure_age_types
  end
end
