class CreateAgeTypes < ActiveRecord::Migration
  def change
    create_table :age_types do |t|
      t.integer :age_range
      t.decimal :price

      t.timestamps null: false
    end
  end
end