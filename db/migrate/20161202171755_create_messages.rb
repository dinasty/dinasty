class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :phone
      t.text :text
      t.datetime :send_time
      t.integer :status

      t.timestamps null: false
    end
  end
end
