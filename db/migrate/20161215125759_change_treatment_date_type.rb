class ChangeTreatmentDateType < ActiveRecord::Migration
  def change
    change_column :treatments, :treatment_date, :date
  end
end
