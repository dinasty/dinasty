class ChangeColumnNameSexToGender < ActiveRecord::Migration
  def change
    rename_column :patients, :sex, :gender
  end
end
