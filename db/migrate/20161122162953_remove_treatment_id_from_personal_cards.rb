class RemoveTreatmentIdFromPersonalCards < ActiveRecord::Migration
  def change
    remove_reference :personal_cards, :treatment, index: true, foreign_key: true
  end
end
