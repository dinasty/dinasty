class AddTreatmentProcessToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :treatment_process, :text
  end
end
