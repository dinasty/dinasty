class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name
      t.text :message
      t.string :email
      t.string :phone
      t.boolean :show

      t.timestamps null: false
    end
  end
end
