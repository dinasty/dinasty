class AddDefaultImageToUser < ActiveRecord::Migration
  def change
    add_column :users, :default_image, :string
  end
end
