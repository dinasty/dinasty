class RemoveColumnsFromTreatments < ActiveRecord::Migration
  def change
    remove_column :treatments, :procedure_id, :integer
  end
end