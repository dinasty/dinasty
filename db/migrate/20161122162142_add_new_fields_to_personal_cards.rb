class AddNewFieldsToPersonalCards < ActiveRecord::Migration
  def change
    add_column :personal_cards, :disease, :string
    add_column :personal_cards, :allergy, :string
    add_column :personal_cards, :medicine, :string
    add_column :personal_cards, :bleeding, :string
    add_column :personal_cards, :pregnancy, :boolean
    add_column :personal_cards, :gestational_age, :string
  end
end
