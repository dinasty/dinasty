class AddColumnToPriceLists < ActiveRecord::Migration
  def change
    add_column :price_lists, :category_id, :integer
    add_index :price_lists, :category_id
  end
end
