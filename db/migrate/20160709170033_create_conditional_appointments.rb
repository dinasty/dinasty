class CreateConditionalAppointments < ActiveRecord::Migration
  def change
    create_table :conditional_appointments do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
