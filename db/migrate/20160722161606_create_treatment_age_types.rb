class CreateTreatmentAgeTypes < ActiveRecord::Migration
  def change
    create_table :treatment_age_types do |t|
      t.references :treatment, index: true, foreign_key: true
      t.references :age_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
