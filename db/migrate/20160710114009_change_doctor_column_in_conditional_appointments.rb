class ChangeDoctorColumnInConditionalAppointments < ActiveRecord::Migration
  def change
    remove_column :conditional_appointments, :doctor, :string
    add_column :conditional_appointments, :doctor_id, :integer
  end
end
