class CreateTreatmentDetails < ActiveRecord::Migration
  def change
    create_table :treatment_details do |t|
      t.references :treatment, index: true, foreign_key: true
      t.integer :tooth
      t.references :procedure, index: true, foreign_key: true
      t.decimal :price

      t.timestamps null: false
    end
  end
end
