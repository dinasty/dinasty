class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.text :message
      t.string :patient_ids, array: true, default: []
      t.integer :sms_id
      t.integer :status
      t.timestamps null: false
    end
  end
end
