class CreatePriceListCategories < ActiveRecord::Migration
  def change
    create_table :price_list_categories do |t|
      t.string :category
      t.text :description

      t.timestamps null: false
    end
  end
end
