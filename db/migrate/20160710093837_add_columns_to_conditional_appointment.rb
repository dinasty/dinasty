class AddColumnsToConditionalAppointment < ActiveRecord::Migration
  def change
    add_column :conditional_appointments, :age, :integer
    add_column :conditional_appointments, :doctor, :string
    add_column :conditional_appointments, :appointment_date, :datetime
    add_column :conditional_appointments, :start_time, :datetime
    add_column :conditional_appointments, :end_time, :datetime
  end
end
