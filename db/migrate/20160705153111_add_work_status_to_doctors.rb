class AddWorkStatusToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :work_status, :integer, default: 0
  end
end
