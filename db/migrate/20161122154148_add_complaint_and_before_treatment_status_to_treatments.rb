class AddComplaintAndBeforeTreatmentStatusToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :complaint, :text
    add_column :treatments, :before_treatment_status, :text
  end
end
