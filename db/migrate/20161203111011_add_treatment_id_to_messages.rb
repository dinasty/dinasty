class AddTreatmentIdToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :treatment_id, :integer
  end
end
