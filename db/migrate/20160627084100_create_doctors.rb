class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.references :user, index: true, foreign_key: true
      t.string :phone
      t.text :work_history
      t.integer :work_experience
      t.string :position

      t.timestamps null: false
    end
  end
end
