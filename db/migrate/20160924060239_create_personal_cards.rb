class CreatePersonalCards < ActiveRecord::Migration
  def change
    create_table :personal_cards do |t|
      t.references :patient, index: true, foreign_key: true
      t.references :treatment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
