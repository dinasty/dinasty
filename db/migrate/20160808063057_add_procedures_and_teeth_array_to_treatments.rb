class AddProceduresAndTeethArrayToTreatments < ActiveRecord::Migration
  def change
    remove_column :treatments, :teeth_number, :string
    add_column :treatments, :teeth, :string, array: true, default: []
    add_column :treatments, :procedures, :string, array: true, default: []
  end
end
