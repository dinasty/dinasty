class RemoveColumnFromAgeTypes < ActiveRecord::Migration
  def change
    remove_column :age_types, :price, :decimal
    add_column :treatment_age_types, :price, :decimal
  end
end
