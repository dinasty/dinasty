class RenamePriceListTable < ActiveRecord::Migration
  def change
    rename_table :price_lists, :treatments
  end
end
