class AddDoctorToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :doctor, index: true, foreign_key: true
  end
end
