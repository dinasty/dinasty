class AddReferenceToDoctorInConditionalAppointments < ActiveRecord::Migration
  def change
    remove_column :conditional_appointments, :doctor_id, :integer
    add_reference :conditional_appointments, :doctor, index: true, foreign_key: true
  end
end
