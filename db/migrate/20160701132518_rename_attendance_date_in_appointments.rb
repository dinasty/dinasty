class RenameAttendanceDateInAppointments < ActiveRecord::Migration
  def change
    rename_column :appointments, :attendance_date, :appointment_date
  end
end
