class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :phone
      t.integer :age
      t.string :sex
      t.integer :status, default: 0
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
