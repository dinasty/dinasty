class RenameAttendancesTable < ActiveRecord::Migration
  def change
    rename_table :attendances, :appointments
  end
end
