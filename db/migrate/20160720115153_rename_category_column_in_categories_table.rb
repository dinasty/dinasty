class RenameCategoryColumnInCategoriesTable < ActiveRecord::Migration
  def change
    rename_column :price_list_categories, :category, :title
  end
end
