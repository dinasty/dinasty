class AddStateToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :state, :integer, default: 0
  end
end
