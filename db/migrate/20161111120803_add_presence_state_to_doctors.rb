class AddPresenceStateToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :presence_state, :integer, default: 0
  end
end
