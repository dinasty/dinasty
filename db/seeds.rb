roles = [:admin, :moderator, :patient, :doctor]
doctor_work_status = [:present, :on_holiday, :on_the_sick, :fired]
country_phone_code = '996'
mobile_operators_phone_codes = [
    '550', '551', '552', '553', '554', '555', '556', '557', '558', '559',
    '770', '771', '772', '773', '774', '775', '776', '777', '778', '779',
    '700', '701', '702', '703', '704', '705'
]
phone_numbers = [0,1,2,3,4,5,6,7,8,9]

gender = [:male, :female]
patient_status = [:active, :inactive]
start_time = ['08:00', '08:30', '09:00', '09:30', '10:00', '10:30']
end_time = ['11:00', '11:30', '12:00', '12:30', '13:00', '13:30']
appointment_status = [:not_approved, :approved, :postponed, :canceled]
age_titles = ['Для детей', 'Для взрослых', 'Для пожилых']
@prices = [300, 350, 450, 500, 520, 580, 610, 750, 700, 760, 800, 850, 910, 940, 1000, 1500, 1200, 2500, 1420]
lower_age = [5, 17, 60]
upper_age = [16, 59, 100]

def create_user
  User.create(
      name: Faker::Name.first_name, surname: Faker::Name.last_name, email: Faker::Internet.safe_email,
      password: '123456789', password_confirmation: '123456789'
  )
end

user = User.create(
    name: 'Admin', surname: 'Adminov', email: 'admin@example.com',
    password: '123456789', password_confirmation: '123456789'
)
user[:default_image] = 'admin_default.ico'
user.save
admin = user.add_role roles[0]


control_user_doctor = User.create(
    name: 'Доктор', surname: 'Контрольный', email: 'doctor@example.com',
    password: '123456789', password_confirmation: '123456789'
)
control_user_doctor[:default_image] = 'doctor_1.jpg'
control_user_doctor.save

control_user_doctor.add_role roles[3]
control_user_doctor.create_doctor(
    phone: '996555111213',
    work_experience: rand(5..15),
    position: 'dentist',
    work_status: doctor_work_status.sample
)


30.times do
  doctor_user = create_user
  doctor_user[:default_image] = 'doctor_1.jpg'
  doctor_user.save

  doctor_user.add_role roles[3]
  doctor_user.create_doctor(
      phone: country_phone_code + mobile_operators_phone_codes.sample + phone_numbers.sample(6).join(''),
      work_experience: rand(5..15),
      position: 'dentist',
      work_status: doctor_work_status.sample
  )
end

70.times do
  patient_user = create_user

  patient_user.add_role roles[2]
  patient = patient_user.create_patient(
      phone: country_phone_code + mobile_operators_phone_codes.sample + phone_numbers.sample(6).join(''),
      age: rand(5..80),
      gender: gender.sample,
      status: patient_status.sample
  )

  patient.create_personal_card
  patient_user[:default_image] = 'patient_default.jpg'
  patient_user.save

  rand(1..3).times do
    patient.appointments.create(
      doctor_id: Doctor.all.sample.id,
      appointment_date: Faker::Date.forward(14),
      start_time: start_time.sample.to_time,
      end_time: end_time.sample.to_time,
      status: appointment_status.sample
    )
  end
end

60.times do
  ConditionalAppointment.create(
    name: Faker::Name.first_name, surname: Faker::Name.last_name,
    email: Faker::Internet.safe_email,
    phone: country_phone_code + mobile_operators_phone_codes.sample + phone_numbers.sample(6).join('')
  )
end


# => Seeds for price-list
categories = [
  'Обезболивание. Анастезия', # => diagnosis_1
  'Терапевтическая стоматология', # => diagnosis_2
  'Лечение пульпитов и периодонтитов', # => diagnosis_3
  'Ортопантомограмма цифровая челюстей', # => diagnosis_4
  'Ортопедическая стоматология', # => diagnosis_5
  'Несъемное протезирование', # => diagnosis_6
  'Съемное протезирование', # => diagnosis_7
  'Пародонтологическая хирургия', # => diagnosis_8
  'Хирургическое лечение одонтогенных воспалительных заболеваний кисти челюстей', # => diagnosis_9
  'Планирование имплантологического лечения' # => diagnosis_10
]

diagnosis_1 = [
  'Аппликационная (гель)',
  'Инфильтрационная',
  'Проводниковая анестезия',
  'Премедикация внутрь (per os)',
  'Комбинированная премедикация (2 и более препаратов)'
]

diagnosis_2 = [
  'Препарирование подготовка полости',
  'Удаление старой пломбы',
  'Медикаментозная обработка полости',
  'Постановка лечебной прокладки',
  'Постановка изолирующей прокладки',
  'Постановка временной пломбы',
  'Светоотверждаемый пломбировочный материал «Te - ekonom» 1 поверхность',
  'Постановка анкерного штифта',
  'Постановка стекловолоконного штифта',
  'Постановка стального штифта'
]

diagnosis_3 = [
  'Вскрытие полости зуба',
  'Механическая и медикаментозная обработка 1-го канала',
  'Девитализация пульпы',
  'Пломбирование 1-го канала пастой',
  'Пломбирование 1-го канала с использованием гуттаперчевых штифтов',
  'Извлечение инородного тела из канала',
  'Вскрытие полости зуба через искусственную коронку',
  'Коагуляция десневого сосочка'
]

diagnosis_4 = [
  'Прицельная внутриротовая рентгенография',
  'Ортопантомограмма цифровая челюстей (на бумаге)',
  'Ортопантомограмма цифровая челюстей (на пленке)'
]

diagnosis_5 = [
  'Снятие штампованной коронки',
  'Снятие цельнолитой коронки',
  'Фиксация на стеклоиномерный цемент (1 единица)',
  'Фиксация на фосфат цемент',
  'Временная фиксация коронки на имплантат',
  'Оттиск альгинатный',
  'Оттиск двухслойный А – силиконовый'
]

diagnosis_6 = [
  'Коронка пластмассовая',
  'Фасетка пластмассовая',
  'Вкладка культевая со штифтом цельнолитая',
  'Коронка металлокерамическая на основе сплавов неблагородных металлов',
  'Коронка металлокерамическая на дентальном импланте (Alpha Bio)',
  'Коронка металлокерамическая на дентальном имплантате (Biohorisons)'
]

diagnosis_7 = [
  'Съемный протез из пластмассы, полный и частичный пластинчатый',
  'Бюгельный протез на кламмерах',
  'Микросъемный протез эстетический нейлоновый',
  'Микросъемный протез эстетический пластмассовый',
  'Бюгельный протез на аттачменах',
  'Нейлоновый протез',
  'Индивидуальная ложка'
]

diagnosis_8 =[
  'Пластика уздечки верхней или нижней губы',
  'Гингивотомия в области 1 зуба',
  'Латеральная ротация лоскута в области 1 зуба',
  'Кюретаж парадонтальных карманов в области 1 зуба',
  'Кюретаж (открытый) парадонтальных карманов в области 1 зуба',
  'Лоскутная операция (с целью санации парадонтального кармана) в области 1 зуба'
]

diagnosis_9 =[
  'Сложное удаление зуба (с разделением корней)',
  'Удаление зуба ( без разделения корней )',
  'Удаление ретинированного, дистопированного или сверхкомплектного зуба',
  'Остановка луночкового кровотечения',
  'Хирургическое лечение альвеолита',
  'Хирургическое лечение перикоронита (иссечение «капюшона»)',
  'Периостотомия (внутриротовой разрез при периостите)'
]

diagnosis_10 =[
  'Изготовление диагностических моделей челюстей с постановкой искусственных зубов в артикуляторе',
  'Изготовление мягкого хирургического шаблона с помощью вакуумформера',
  'Использование винтового, корневидного имплантата (BioHorisons) СПЕЦПРЕДЛОЖЕНИЕ',
  'Использование винтового, корневидного имплантата (Replace select)',
  'Использование винтового, корневидного имплантата (Alpha Bio)',
  'Применение методики направленной костной регенерации с использованием резорбируемой мембраны (без стоимости мембраны)'
]

def creating_age_type(title, lower, upper)
  AgeType.create title: title, lower_age: lower, upper_age: upper, constant_name: 'adult'
end

creating_age_type('Взрослые', lower_age[1], upper_age[1])

# age_titles.each do |title|
#   # if title == 'Для детей'
#   #   creating_age_type(title, lower_age[0], upper_age[0])
#   if title == 'Взрослые'
#     creating_age_type(title, lower_age[1], upper_age[1])
#   # else
#   #   creating_age_type(title, lower_age[2], upper_age[2])
#   end
# end

def creating_price_list(category, diagnosis)
  category= PriceListCategory.create(title: category)

  diagnosis.each do |diagnosis|
    procedure = category.procedures.create(title: diagnosis)
    AgeType.all.each do |type|
      procedure.procedure_age_types.create(age_type: type, price: @prices.sample)
    end
  end
end

creating_price_list(categories[0], diagnosis_1)
creating_price_list(categories[1], diagnosis_2)
creating_price_list(categories[2], diagnosis_3)
creating_price_list(categories[3], diagnosis_4)
creating_price_list(categories[4], diagnosis_5)
creating_price_list(categories[5], diagnosis_6)
creating_price_list(categories[6], diagnosis_7)
creating_price_list(categories[7], diagnosis_8)
creating_price_list(categories[8], diagnosis_9)
creating_price_list(categories[9], diagnosis_10)