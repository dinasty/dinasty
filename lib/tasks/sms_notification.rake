namespace :sms do
  task send: :environment do
    messages = Message.where(:send_time => Date.today.beginning_of_day..Date.today.end_of_day, status: 'created')
    puts "messages count: #{messages.count}"
    puts "Time: #{Time.now}"
    if messages.present?
      http = Net::HTTP.new('smspro.nikita.kg')
      messages.each do |message|
        resp = http.post('/api/message', to_xml(message))
        response = Hash.from_xml(resp.body)['response']['status']
        message.update(sms_id: message.id.to_s.rjust(7, 'D00000'), status: 1) if response == '0' || response == '11'
      end
    end
  end

  def to_xml message
    build = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.message {
        xml.login ENV["SMS_NIKITA_LOGIN"]
        xml.pwd ENV["SMS_NIKITA_PASS"]
        xml.id message.id.to_s.rjust(7, 'D00000')
        xml.sender "dinastiya"
        xml.text_ "Напоминаем, завтра у Вас назначен прием в #{message.treatment.start_time.strftime("%H:%M")}. Стоматология Династия"
        # xml.time self.send_time.strftime("%Y%m%d%H%M%S")
        xml.phones {
          xml.phone message.phone
        }
        # xml.test '1'
      }
    end
    build.to_xml
  end

end
