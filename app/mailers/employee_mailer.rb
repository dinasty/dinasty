class EmployeeMailer < ApplicationMailer
  default from: 'dinastiyakg@gmail.com'
 
  def send_password doctor, password
    @doctor = doctor
    @password = password
    @url = 'http://dinastiya.kg/users/sign_in'
    mail(to: doctor.email, subject: 'Стоматология Династия. Подтверждение регистрации в системе')
  end
end