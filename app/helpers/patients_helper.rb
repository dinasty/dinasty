module PatientsHelper

  def patients_amount
    if current_user.doctor?
      patient_ids = current_user.doctor.treatments.map(&:patient_id)
    end

    current_user.doctor? ? patient_ids.uniq.count : Patient.all.count
  end

  def last_treatment patient
    treatment = patient.treatments.present.
        where.not(treatment_date: nil).
        where(
            'treatment_date <= ? AND state = ?',
            Date.today, Treatment.states[:hosted]
        ).last

    treatment
  end

  def past_treatments_count patient
    patient.treatments.present.where(
        'treatment_date <= ? AND state = ?',
        Date.today, Treatment.states[:hosted]
    ).count
  end

  def future_treatments patient
    patient.treatments.present.where(
        'treatment_date >= ? AND state != ?',
        Date.today, Treatment.states[:hosted]
    )
  end

  def patient_doctors patient
    doctors_ids = patient.treatments.map(&:doctor_id).compact
    Doctor.where(id: doctors_ids)
  end

  def hosted_treatments patient
    # if current_user.doctor?
    #   patient.treatments.present.where(
    #       state: Treatment.states[:hosted],
    #       doctor_id: current_user.doctor.id
    #   )
    # else
      patient.treatments.present.where(state: Treatment.states[:hosted])
    # end
  end

  def cured_teeth patient
    patient.treatments.where(state: Treatment.states[:hosted]).map(&:teeth).flatten.uniq
  end

  def spent_money patient
    patient.treatments.where(state: Treatment.states[:hosted]).sum(:price).to_i
  end

  def sidebar_user_avatar user
    asset_path user.avatar.present? ? user.avatar : user.default_image
  end
end