module MessagesHelper
  def nikita_response(resp)
    case resp
    when '0'
      flash[:success] = 'Сообщение успешно отправлено'

    when '1'
      flash[:error] = 'Ошибка в формате запроса'

    when '2'
      flash[:error] = 'Неверная авторизация'

    when '3'
      flash[:error] = 'Недопустимый IP‐адрес отправителя'

    when '4'
      flash[:error] = 'Недостаточно средств на счету'

    when '5'
      flash[:error] = 'Недопустимое имя отправителя'

    when '6'
      flash[:error] = 'Сообщение заблокировано по стоп‐словам'

    when '7'
      flash[:error] = ' Некорректное написание одного или нескольких номеров'

    when '8'
      flash[:error] = 'Неверный формат времени отправки'

    when '9'
      flash[:error] = 'Отправка заблокирована из‐за срабатывания SPAM фильтра.'

    when '10'
      flash[:error] = 'Отправка заблокирована  из‐за последовательного повторения id'

    when '11'
      flash[:warning] = 'Тестовое Сообщение успешно обработано'
    else
      flash[:error] = 'Ошибка отправки'

    end
  end

  def show_messages_statuses(message)
    if message.created?
      %w(gray Создано)
    elsif message.sent?
      %w(yellow Отправлено)
    elsif message.delivered?
      %w(green Доставлено)
    else
      ['red', 'Не отправлено']
    end
  end
end
