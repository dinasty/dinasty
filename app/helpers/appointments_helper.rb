module AppointmentsHelper

  def show_statuses_of appointment
    if appointment.not_approved?
      return ['gray', 'Не подтвержден']
    elsif appointment.approved?
      return ['green', 'Подтвержден']
    elsif appointment.postponed?
      return ['red', 'Отложен']
    elsif appointment.canceled?
      return ['black', 'Отменен']
    end
  end

  def appointments_amount
    current_user.doctor? ? current_user.doctor.appointments.count : Appointment.all.count
  end

end