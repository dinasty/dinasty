module ConditionalAppointmentsHelper

  def show_statuses appointment
    if appointment.waiting?
      return ['green', 'Новая']
    elsif appointment.processed?
      return ['gray', 'Обработана']
    elsif appointment.canceled?
      return ['black', 'Отменена']
    end
  end

  def conditional_appointments_amount
    current_user.doctor? ?
        current_user.doctor.conditional_appointments.count :
        ConditionalAppointment.all.count
  end

end