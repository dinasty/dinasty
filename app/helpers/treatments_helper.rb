module TreatmentsHelper

  def show_treatments_statuses treatment
    if treatment.unstarted?
      return ['gray', 'Новый']
    elsif treatment.in_process?
      return ['yellow', 'В процессе']
    elsif treatment.hosted?
      return ['green', 'Завершен']
    elsif treatment.postponed?
      return ['red', 'Отложен']
    elsif treatment.canceled?
      return ['black', 'Отменен']
    end
  end

  def price treatment
    treatment.price.to_i.round(0)
  end

  def treatments_amount patient
    current_user.doctor? ?
        current_user.doctor.treatments.present.where(patient_id: patient.id).count :
        patient.treatments.present.count
  end

end
