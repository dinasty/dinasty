module FeedbacksHelper

  def show_message feedback
    if feedback.show
      'Скрыть'
    else
      'Показать'
    end
  end
end
