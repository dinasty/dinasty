class Invoice < Prawn::Document

  def initialize(treatment)
    super()
    @treatment = treatment
    @patient = treatment.patient
    @treatment_details = treatment.treatment_details
    add_font
    header
    body
    doctor
    recomendation
    footer
  end

  def add_font
    font_families.update("ubuntu" => {
        :normal => "app/assets/fonts/regular.ttf",
        :italic => "app/assets/fonts/italic.ttf",
        :bold => "app/assets/fonts/bold.ttf",
    })
    font "ubuntu",
         :size => 14
  end

  def header
    image Rails.root.join('app','assets','images','logo_pdf.jpg').to_s

    move_down 10
    text "бульвар Эркиндик 40, пер. Киевская. Тел: (0312)881999, (0700)811999",
         align: :right, size: 11

    move_down 20
    text "Акт оказанных услуг ",
         align: :center, size: 20

  end

  def body

    move_down 20
    text "ФИО: #{@patient.fullname}"

    move_down 5
    text "Возраст: #{@patient.age if @patient.age.present?}"

    move_down 5
    text "Место работы, должность:____________________________________________________"
    text "______________________________________________________________________________"

    move_down 10
    text "Жалобы: #{@treatment.complaint}"
    text "______________________________________________________________________________"

    move_down 10
    text "Объективно:__________________________________________________________________"
    text "______________________________________________________________________________"

    move_down 10
    text "DS:___________________________________________________________________________"
    text "______________________________________________________________________________"

    move_down 20
    text "Лечение:"

    move_down 20
    data = [["<b>Зуб</b>", "<b>Процедура</b>", "<b>Цена</b>"]]
    @treatment_details.each do |detail|
        data += [["#{detail.tooth}",
                  "#{detail.procedure.title}",
                  "#{detail.price.round(1)} сом"]]
    end
    table(data, :header => true, :width => 540, :cell_style => {align: :left, size: 10, :inline_format => true})

    move_down 15
    text "Сумма: #{@treatment.price} ", align: :right, style: :bold, size: 14
  end

  def doctor
    move_down 25
    text "Врач: #{@treatment.doctor.fullname}",
         :align => :left

    move_down 20
    text "Дата приема: #{@treatment.treatment_date.strftime("%d %m %Y")}",
         :align => :left

  end

  def recomendation
    move_down 20
    text "Рекомендации:", align: :left, style: :bold
    text "______________________________________________________________________________"
    text "______________________________________________________________________________"
    text "______________________________________________________________________________"
  end

  def footer
    move_down 50
    text "Гарантия на все виды работы 1 год.",
         align: :center, size: 14, style: :bold
    move_down 2

    # text "Лицензия",
    #      align: :left, size: 14
  end

end