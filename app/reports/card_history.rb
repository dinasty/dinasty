class CardHistory < Prawn::Document

  def initialize(patient, personal_card)
    super()
    @patient = patient
    @personal_card = personal_card
    @treatments = patient.treatments.where(state: Treatment.states[:hosted])
    @hosted_treatments = TreatmentDetail.hosted_treatments @patient
    add_font
    header
    body
    treatment_details
    footer

  end

  def add_font
    font_families.update("ubuntu" => {
        :normal => "app/assets/fonts/regular.ttf",
        :italic => "app/assets/fonts/italic.ttf",
        :bold => "app/assets/fonts/bold.ttf",
    })
    font "ubuntu",
         :size => 14
  end

  def header
    image Rails.root.join('app', 'assets', 'images', 'logo_pdf.jpg').to_s

    move_down 10
    text "бульвар Эркиндик 40, пер. Киевская. Тел: (0312)881999, (0700)811999",
         align: :right, size: 11

    move_down 20
    text "Амбулаторная медицинская карта пациента",
         align: :center, size: 20

  end

  def body
    move_down 20
    text "ФИО: #{@patient.fullname}"

    move_down 5
    text "Возраст: #{@patient.age if @patient.age.present?}"

    move_down 5
    text "Место работы, должность:____________________________________________________"
    text "______________________________________________________________________________"

    move_down 5
    text "Телефон: #{@patient.phone}"

    move_down 5
    text "Адрес:________________________________________________________________________"

    move_down 5
    text "Дата обращения:_____________________________________________________________"

    move_down 5
    text "Болезни: #{@personal_card.disease}"

    move_down 5
    text "Аллергия: #{@personal_card.allergy}"

    move_down 5
    text "Прием лекарств: #{@personal_card.medicine}"

    move_down 5
    text "Кровотечение: #{@personal_card.bleeding}"

    if @personal_card.pregnancy
      move_down 5
      text "Беременность: #{@personal_card.pregnancy}, срок беремнности: #{@personal_card.gestational_age} "
    end

  end

  def treatment_details
    move_down 20
    text "Лечение:"

    move_down 20
    data = [["<b>Врач</b>", "<b>Зуб</b>", "<b>Процедура</b>", "<b>Дата приема</b>", "<b>Цена</b>"]]
    @hosted_treatments.each do |detail|
      doctor = detail.treatment.doctor
      date = detail.treatment.treatment_date.strftime("%d.%m.%Y")
      data += [["#{doctor.fullname}",
                "#{detail.tooth}",
                "#{detail.procedure.title}",
                "#{date}",
                "#{detail.price.round(1)} сом"
               ]]
    end
    table(data, :header => true, :width => 540, :cell_style => {align: :left, size: 10, :inline_format => true})

    move_down 15
    text "Сумма: #{@treatments.sum(:price).round(1)} сом ", align: :right, style: :bold, size: 12
  end

  def footer
    move_down 50
    text "Гарантия на все виды работы 1 год.",
         align: :center, size: 14, style: :bold
    move_down 2

    # text "Лицензия",
    #      align: :left, size: 14
  end

end