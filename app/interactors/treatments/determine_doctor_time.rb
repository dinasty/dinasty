module Treatments
  class DetermineDoctorTime
    include Interactor

    def call
      doctor = Doctor.find(context.doctor_id)
      date_treatments = doctor.treatments.where(treatment_date: context.treatment_date.to_date)
      disabled_hours = []

      date_treatments.each do |treatment|
        time_difference = treatment.end_time - treatment.start_time
        start_time = treatment.start_time.hour
        end_time = treatment.end_time.hour

        if time_difference <= 1.hour
          if treatment.start_time.strftime('%M') == '30' and treatment.end_time.strftime('%M') == '30'
            disabled_hours << start_time << end_time
          else
            disabled_hours << start_time
          end

        elsif time_difference > 1.hour and time_difference < 2.hours
          if treatment.start_time.strftime('%M') == '30'
            disabled_hours << (start_time...end_time).to_a
          else
            disabled_hours << start_time << end_time
          end

        elsif time_difference == 2.hours or time_difference == 3.hours
          if treatment.start_time.strftime('%M') == '30' and treatment.end_time.strftime('%M') == '30'
            disabled_hours << (start_time..end_time).to_a
          else
            disabled_hours << (start_time...end_time).to_a
          end

        elsif time_difference > 2.hours and time_difference < 3.hours
          if treatment.start_time.strftime('%M') == '30'
            disabled_hours << (start_time...end_time).to_a
          else
            disabled_hours << (start_time..end_time).to_a
          end
        end
      end

      context.resource = disabled_hours.flatten.uniq
    end
  end
end