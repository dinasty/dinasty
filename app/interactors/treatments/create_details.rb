module Treatments
  class CreateDetails
    include Interactor

    def call
      if context.treatment.treatment_details
        context.treatment.treatment_details.destroy_all
      end

      context.therapy.each do |element|
        element.each do |key, value|
          value.each do |procedure_id|
            procedure = Procedure.find(procedure_id)
            adult = procedure.age_types.where(constant_name: 'adult').first

            if context.age_type
              procedure_price = procedure.procedure_age_types.
                  where(age_type_id: context.age_type.id).first
              if procedure_price.nil?
                procedure_price = procedure.procedure_age_types.where(age_type_id: adult.id).first
              end
            else
              procedure_price = procedure.procedure_age_types.where(age_type_id: adult.id).first
            end

            context.treatment.treatment_details.create(
                tooth: key, procedure_id: procedure_id.to_i, price: procedure_price.price
            )
          end
        end
      end
    end

  end
end