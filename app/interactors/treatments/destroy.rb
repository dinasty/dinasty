module Treatments
  class Destroy
    include Interactor

    def call
      begin
        treatment = Treatment.find_by!(id: context.treatment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Прием не найден. Удаление не завершено'}
      end

      patient = treatment.patient
      treatment.update_attribute :presence_state, Treatment.presence_states[:removed]

      if treatment.valid?
        context.resource = patient
        context.message = {success: 'Прием успешно удален'}
      end
    end
  end
end