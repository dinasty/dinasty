module Treatments
  class List
    include Interactor

    def call
      user = context.current_user
      sort = context.sort
      direction = context.direction

      treatments = user.doctor? ?
          context.patient.treatments.present.where(doctor_id: user.doctor.id) :
           context.patient.treatments.present

      if sort and direction
        if sort == 'treatment_date'
          result = treatments.order(sort => direction)
        elsif sort == 'doctor_surname'
          result = treatments.includes(:doctor, doctor: [:user]).order("users.surname #{direction}")
        elsif sort == 'price'
          result = treatments.order(sort => direction)
        end
      else
        result = treatments.order(treatment_date: :asc)
      end

      context.resources = result.page(context.page)
    end
  end
end