module Treatments
  class ManageStatuses
    include Interactor

    def call
      begin
        treatment = Treatment.find(context.treatment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Прием не найден. Обновление не завершено'}
      end

      treatment.update_attributes(context.permitted_params[:treatment])

      if treatment.valid?
        context.resource = treatment
        context.message = {success: 'Информация была успешно обновлена'}
      end
    end
  end
end