module Treatments
  class AgePriceCalculatings
    include Interactor

    def call
      age_type = context.age_type

      prices_array = context.procedures.map do |id|
        procedure = Procedure.find(id)
        adult = procedure.age_types.where(constant_name: 'adult').first

        if age_type
          begin
            procedure.procedure_age_types.find_by!(age_type_id: age_type.id).price
          rescue
            procedure.procedure_age_types.where(age_type_id: adult.id).first.price
          end
        else
          procedure.procedure_age_types.order(price: :desc).first.price
        end
      end

      context.resource = prices_array
    end
  end
end