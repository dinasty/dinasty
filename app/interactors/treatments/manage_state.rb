module Treatments
  class ManageState
    include Interactor

    def call
      begin
        treatment = Treatment.find(context.treatment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Прием не найден. Обновление не завершено'}
      end

      treatment.update_attribute :state, context.treatment_state
      context.resource = treatment.patient

      if treatment.state == 'hosted'
        context.message = {success: 'Прием успешно завершен'}
      else
        context.message = {success: 'Статус приема успешно обновлен'}
      end
    end
  end
end