module Treatments
  class Create
    include Interactor

    def call
      params = context.assembled_params
      params[:treatment_date].to_datetime

      begin
        patient = Patient.find(context.patient_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Пациент не найден. Обновление не завершено'}
      end

      age_type = patient.age.present? ?
          AgeType.where('lower_age <= ? and upper_age >= ?', patient.age, patient.age).first : nil

      result_price = Treatments::AgePriceCalculatings.call(
          procedures: params[:procedures], age_type: age_type
      )
      treatment = patient.treatments.create(params.except(:treatment_id))
      treatment.update(price: result_price.resource.sum, end_time: treatment.start_time + context.duration.to_i,)

      if treatment.valid?
        if treatment.messages.present?
          if treatment.treatment_date != (treatment.messages.first.send_time + 1.day)
            treatment.messages.first.update(
                send_time: treatment.treatment_date - 1.day,
                phone: treatment.patient.phone,
                status: :created
            )
          end
        else
          treatment.messages.create(
              send_time: treatment.treatment_date - 1.day,
              phone: treatment.patient.phone,
              status: :created
          )
        end

        result = Treatments::CreateDetails.call(
            therapy: context.therapy,
            treatment: treatment,
            age_type: age_type
        )

        if result.success?
          context.resource = patient
          context.message = {success: 'Информация о приеме успешно добавлена'}
        end
      end
    end
  end
end