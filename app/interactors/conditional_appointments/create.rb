module ConditionalAppointments
  class Create
    include Interactor
    
    def call
      begin
        conditional_app = ConditionalAppointment.create(context.permitted_params)
      rescue ActiveRecord::RecordInvalid
        context.fail! message: {error: 'Временно невозможно создать запись на первичный осмотр. Пожалуйста, попробуйте позднее'}
      end

      if conditional_app.valid?
        context.resource = conditional_app
        context.message = {success: 'Запись на первичный осмотр успешно создана'}
      end
    end
  end
end