module ConditionalAppointments
  class List
    include Interactor

    def call
      user = context.current_user
      sort = context.sort
      direction = context.direction

      appointments = user.doctor? ? user.doctor.conditional_appointments : ConditionalAppointment

      if sort and direction
        result = appointments.order(sort => direction, created_at: :desc)
      else
        result = appointments.order(appointment_status: :asc, created_at: :desc)
      end

      context.resources = result.page(context.page)
    end
  end
end


