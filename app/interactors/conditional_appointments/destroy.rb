module ConditionalAppointments
  class Destroy
    include Interactor

    def call
      begin
        conditional_app = ConditionalAppointment.find(context.conditional_app_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Запись на первичный осмотр не найдена. Удаление не завершено'}
      end

      conditional_app.destroy
      context.message = {success: 'Запись на первичный осмотр успешно удалена'}
    end
  end
end