module ConditionalAppointments
  class ChangeStatus
    include Interactor

    def call
      begin
        appointment = ConditionalAppointment.find(context.appointment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Запись на прием не найдена. Обновление не завершено'}
      end

      appointment.update_attribute :appointment_status, context.status

      if appointment.canceled?
        appointment.update_attribute :processed_time, Time.now
      elsif appointment.waiting?
        appointment.update_attribute :processed_time, nil
      end

      context.resource = appointment
      context.message = {success: 'Статус успешно обновлен'}
    end
  end
end