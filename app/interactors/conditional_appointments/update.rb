module ConditionalAppointments
  class Update
    include Interactor

    def call
      begin
        conditional_app = ConditionalAppointment.find(context.conditional_app_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Запись на первичный осмотр не найдена. Обновление не завершено'}
      end

      result = Patients::CreateUserForPatient.call(user_params: context.user_params)
      user = result.resource if result.success?

      if user
        user.add_role :patient
        patient = user.create_patient(context.patient_params)
        patient.create_personal_card if patient.valid?
        appointment = patient.appointments.create(context.appointment_params) if patient.valid?
      end

      if appointment.valid?
        conditional_app.update_attributes(
            appointment_status: :processed,
            processed_time: Time.now,
            doctor_id: appointment.doctor_id,
            appointment_date: appointment.appointment_date,
            start_time: appointment.start_time,
            age: context.patient_params['age']
        )

        context.resource = appointment
        context.message = {success: 'Запись на первичный осмотр успешно обновлена'}
      end
    end
  end
end