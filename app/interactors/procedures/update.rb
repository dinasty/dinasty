module Procedures
  class Update
    include Interactor

    def call
      begin
        procedure = Procedure.find(context.procedure_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Процедура не найдена. Обновление не завершено'}
      end

      procedure.update_attributes(context.permitted_params)

      if context.prices_params.present?
        context.prices_params.each do |price|
          begin
            procedure_to_update = procedure.procedure_age_types.find_by!(age_type_id: price.first)
            procedure_to_update.update_attributes(age_type_id: price.first, price: price.second.to_d)
          rescue
            procedure.procedure_age_types.create(age_type_id: price.first, price: price.second.to_d)
          end
        end
      end

      if procedure.valid?
        context.resource = procedure
        context.message = {success: 'Процедура успешно обновлена'}
      end
    end
  end
end