module Procedures
  class Create
    include Interactor

    def call
      begin
        procedure = Procedure.create(context.permitted_params)
      rescue ActiveRecord::RecordInvalid
        context.fail! message: {error: 'Ошибка при создании процедуры'}
      end

      if procedure.valid?
        if context.prices_params.present?
          context.prices_params.each do |price|
            procedure.procedure_age_types.create(
                age_type_id: price.first,
                price: price.second.to_d
            )
          end
        end

        context.resource = procedure
        context.message = {success: 'Процедура успешно создана'}
      end
    end
  end
end