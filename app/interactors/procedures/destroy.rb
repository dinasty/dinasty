module Procedures
  class Destroy
    include Interactor

    def call
      begin
        procedure = Procedure.find(context.procedure_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Процедура не найдена. Удаление не завершено'}
      end

      begin
        procedure.destroy
        context.message = {success: 'Процедура успешно удалена'}
      rescue
        context.fail! message: {error: 'Не может быть удалена. Имеются связи с другими объектами системы'}
      end
    end
  end
end