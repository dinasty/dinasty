module Procedures
  class List
    include Interactor

    def call
      sort = context.sort
      direction = context.direction
      page = context.page

      if sort and direction
        context.resources = Procedure.all.order(title: direction).page(page)
      else
        context.resources = Procedure.all.order(title: :asc).
            includes(:procedure_age_types, :age_types, :category).
            page(page)
      end
    end
  end
end