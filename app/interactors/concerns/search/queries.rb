module Concerns::Search::Queries

  private

  def users query
    User.where(["lower(surname) || lower(name) || lower(name) || lower(surname) || lower(email) ||
                'lower(surname) + lower(name)' || 'lower(name) + lower(surname)' LIKE ?",
                "%#{query.mb_chars.downcase.delete(' ').to_s}%"])
  end

  def user_doctors query, users
    doctors = Doctor.where(["phone || lower(position) LIKE ?", "%#{query.downcase}%"])
    Doctor.where('user_id IN (?) OR id IN (?)', users.ids, doctors.ids)
  end

  def user_patients query, users, patients: nil
    patient_to_search = patients.present? ? patients : Patient
    found_patients = patient_to_search.where(["phone LIKE ?", "%#{query.mb_chars.downcase}%"])
    patient_to_search.where('user_id IN (?) OR id IN (?)', users.ids, found_patients.ids)
  end
end