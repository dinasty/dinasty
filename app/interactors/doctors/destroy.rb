module Doctors
  class Destroy
    include Interactor

    def call
      begin
        doctor = Doctor.find(context.doctor_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Доктор не найден. Удаление не завершено'}
      end

      begin
        doctor.destroy
      rescue
        context.fail! message: {error: 'Не может быть удален. Имеются связи с другими объектами системы'}
      end

      doctor_user = doctor.user
      doctor_user.destroy
      context.message = {success: 'Доктор успешно удален из системы'}
    end
  end
end