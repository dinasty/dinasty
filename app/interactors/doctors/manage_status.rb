module Doctors
  class ManageStatus
    include Interactor

    def call
      begin
        doctor = Doctor.find(context.doctor_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Доктор не найден. Обновление статуса не завершено'}
      end

      doctor.update_attribute :work_status, context.work_status

      context.resource = doctor
      context.message = { success: 'Статус успешно обновлен' }
    end
  end
end