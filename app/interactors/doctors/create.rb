module Doctors
  class Create
    include Interactor

    def call
      password = User.generate_password
      params = context.user_params.merge({ password: password, password_confirmation: password })  
      
      user = params[:avatar].nil? ?
          User.create(params.merge(default_image: Doctor::DEFAULT_IMAGE)) : 
          User.create(params)
    
      unless user.valid?
        raise ActiveRecord::RecordNotUnique.new(user.errors.messages.values.flatten.first)
      end

      doctor = user.create_doctor(context.doctor_params)
    
      if doctor.valid?
        EmployeeMailer.send_password(doctor, password).deliver_now

        context.resource = doctor
        context.message = {success: 'Доктор успешно зарегистрирован'}
      end
    end
  end
end