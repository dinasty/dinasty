module Doctors
  class Update
    include Interactor

    def call
      begin
        doctor = Doctor.find(context.doctor_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Доктор не найден. Обновление не завершено'}
      end

      doctor.user.update_attributes(context.user_params)
      doctor.update_attributes(context.doctor_params)

      if doctor.valid?
        context.resource = doctor
        context.message = {success: 'Данные доктора успешно обновлены'}
      else
        raise ActiveRecord::RecordNotUnique.new(doctor.errors.messages.values.flatten.first)
      end
    end
  end
end