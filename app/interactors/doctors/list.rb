module Doctors
  class List
    include Interactor

    def call
      sort = context.sort
      direction = context.direction
      page = context.page

      if sort and direction
        if sort == 'surname'
          context.resources = Doctor.all.working.includes(:user).
              order("users.surname #{direction}").page(page)
        end

      else
        context.resources = Doctor.includes(:user).order(work_status: :asc).page(page)
      end
    end
  end
end