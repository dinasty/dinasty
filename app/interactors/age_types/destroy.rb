module AgeTypes
  class Destroy
    include Interactor

    def call
      begin
        age_type = AgeType.find(context.age_type_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Ошибка при удалении возрастной категории'}
      end

      if age_type.constant_name.present?
        context.fail! message: {error: "Категория Взрослые не может быть удалена"}
      end

      begin
        age_type.destroy
        context.message = {success: 'Возрастная категория успешно удалена'}
      rescue
        context.fail! message: {error: 'Категория не может быть удалена. Имеются зависимые процедуры'}
      end
    end
  end
end