module AgeTypes
  class Create
    include Interactor

    def call
      begin
        age_type = AgeType.create(context.permitted_params)
      rescue ActiveRecord::RecordInvalid
        context.fail! message: {error: 'Ошибка при создании возрастной категории'}
      end

      if age_type.valid?
        context.resource = age_type
        context.message = {success: 'Возрастная категория успешно создана'}
      end
    end
  end
end