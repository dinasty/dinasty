module AgeTypes
  class Update
    include Interactor

    def call
      begin
        age_type = AgeType.find(context.age_type_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Ошибка при обновлении возрастной категории'}
      end

      age_type.update_attributes(context.permitted_params)

      if age_type.valid?
        context.resource = age_type
        context.message = {success: 'Возрастная категория успешно обновлена'}
      end
    end
  end
end