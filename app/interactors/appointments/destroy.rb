module Appointments
  class Destroy
    include Interactor

    def call
      begin
        appointment = Appointment.find(context.appointment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Первичный осмотр не найден. Удаление не завершено'}
      end

      appointment.destroy
      context.message = {success: 'Первичный осмотр успешно удален'}
    end
  end
end