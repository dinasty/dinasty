module Appointments
  class ChangeStatus
    include Interactor

    def call
      begin
        appointment = Appointment.find(context.appointment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Первичный осмотр не найден. Обновление не завершено'}
      end

      appointment.update_attribute :status, context.status

      context.resource = appointment
      context.message = {success: 'Статус успешно обновлен'}
    end
  end
end