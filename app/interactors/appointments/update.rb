module Appointments
  class Update
    include Interactor

    def call
      begin
        appointment = Appointment.find(context.appointment_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Ошибка при обновлении первичного осмотра'}
      end

      appointment.update_attributes(context.permitted_params)

      if appointment.valid?
        context.resource = appointment
        context.message = {success: 'Первичный осмотр успешно обновлен'}
      end
    end
  end
end