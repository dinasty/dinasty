module Appointments
  class Create
    include Interactor

    def call
      begin
        appointment = Appointment.create(context.permitted_params)
      rescue ActiveRecord::RecordInvalid
        context.fail! message: {error: 'Ошибка при создании первичного осмотра'}
      end

      if appointment.valid?
        context.resource = appointment
        context.message = {success: 'Первичный осмотр успешно создан'}
      end
    end
  end
end