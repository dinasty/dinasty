module Appointments
  class List
    include Interactor

    def call
      user = context.current_user
      sort = context.sort
      direction = context.direction

      appointments = user.doctor? ? user.doctor.appointments : Appointment

      if sort and direction
        if sort == 'patient_surname'
          result = appointments.includes(:patient, [patient: [:user]]).
              order("users.surname #{direction}")

        elsif sort == 'doctor_surname'
          result = appointments.includes(:doctor, [doctor: [:user]]).
              order("users.surname #{direction}")

        else
          result = appointments.order(sort => direction, created_at: :desc)
        end
      else
        result = appointments.includes(:patient, :doctor, [patient: [:user]], [doctor: [:user]]).
            order(created_at: :desc)
      end

      context.resources = result.page(context.page)
    end
  end
end