module PersonalCards
  class Update
    include Interactor

    def call
      begin
        personal_card = PersonalCard.find(context.personal_card_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Амбулаторная карта не найдена. Обновление не завершено'}
      end

      personal_card.update_attributes(context.personal_card_params)

      if personal_card.valid?
        context.resource = personal_card
        context.message = {success: 'Амбулаторные данные пациента успешно обновлены'}
      end
    end
  end
end