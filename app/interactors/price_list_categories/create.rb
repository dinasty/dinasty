module PriceListCategories
  class Create
    include Interactor

    def call
      begin
        category = PriceListCategory.create(context.permitted_params)
      rescue ActiveRecord::RecordInvalid
        context.fail! message: {error: 'Ошибка при создании новой категории'}
      end

      if category.valid?
        context.resource = category
        context.message = {success: 'Категория успешно создана'}
      end
    end
  end
end