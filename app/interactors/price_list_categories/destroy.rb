module PriceListCategories
  class Destroy
    include Interactor

    def call
      begin
        category = PriceListCategory.find(context.category_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Категория не найдена. Удаление не завершено'}
      end

      if category.procedures.present?
        context.fail! message: {error: 'Не может быть удалена. Имеются зависимые процедуры'}
      else
        category.destroy
        context.message = {success: 'Категория успешно удалена'}
      end
    end
  end
end