module PriceListCategories
  class Update
    include Interactor

    def call
      begin
        category = PriceListCategory.find(context.category_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Категория не найдена. Обновление не завершено'}
      end

      category.update_attributes(context.permitted_params)

      if category.valid?
        context.resource = category
        context.message = {success: 'Категория успешно обновлена'}
      end
    end
  end
end