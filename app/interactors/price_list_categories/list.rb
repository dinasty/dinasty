module PriceListCategories
  class List
    include Interactor

    def call
      sort = context.sort
      direction = context.direction

      if sort and direction
        context.resources = PriceListCategory.all.order(sort => direction).
            includes(:procedures, [procedures: :age_types])
      else
        context.resources = PriceListCategory.all.order(title: :asc).
            includes(:procedures, [procedures: :age_types])
      end
    end
  end
end