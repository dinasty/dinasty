module Patients
  class Update
    include Interactor

    def call
      begin
        patient = Patient.find(context.patient_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Пациент не найден. Обновление не завершено'}
      end

      patient.update_attributes(context.patient_params)
      patient.user.update_attributes(context.user_params)

      if patient.valid?
        context.resource = patient
        context.message = {success: 'Данные пациента успешно обновлены'}
      else
        raise ActiveRecord::RecordNotUnique.new(patient.errors.messages.values.flatten.first)
      end

    end
  end
end