module Patients
  class ChangeStatus
    include Interactor

    def call
      begin
        patient = Patient.find(context.patient_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Пациент не найден. Обновление статуса не завершено'}
      end

      if patient.active?
        patient.update_attribute :status, :inactive
      elsif patient.inactive?
        patient.update_attribute :status, :active
      end

      if patient.valid?
        context.resource = patient
        context.message = {success: 'Статус был успешно обновлен'}
      end
    end
  end
end