module Patients
  class CreateUserForPatient
    include Interactor

    def call
      user_params = context.user_params

      if user_params[:avatar].nil?
        user = User.create(user_params.merge(default_image: Patient::DEFAULT_AVATAR))
      else
        user = User.create(user_params)
      end

      if user.valid?
        context.resource = user
      else
        raise ActiveRecord::RecordNotUnique.new(user.errors.messages.values.flatten.first)
      end
    end
  end
end