module Patients
  class List
    include Interactor

    def call
      user = context.current_user
      sort = context.sort
      direction = context.direction

      if user.doctor?
        patient_ids = user.doctor.treatments.map(&:patient_id)
        doctor_patients = Patient.where(id: patient_ids)
      end

      patients = user.doctor? ? doctor_patients : Patient

      if sort and direction
        if sort == 'surname'
          result = patients.present.includes(:user).order("users.surname #{direction}")
        elsif sort == 'age'
          result = patients.present.order(age: direction)
        end
      else
        result = patients.present.includes(:user).order(id: :desc)
      end

      context.resources = result.page(context.page)
    end
  end
end