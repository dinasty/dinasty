module Patients
  class Create
    include Interactor

    def call
      user_params = context.user_params
      patient_params = context.patient_params

      result = Patients::CreateUserForPatient.call(
         user_params: user_params,
         patient_params: patient_params
      )

      if result.success?
        patient = result.resource.create_patient(patient_params)
      end

      if patient.valid?
        patient.create_personal_card
        context.resource = patient
        context.message = {success: 'Новый пациент успешно зарегистрирован. Создана медицинская карта'}
      else
        context.fail! message: { error: 'Ошибка при регистрации нового клиента' }
      end
    end
  end
end