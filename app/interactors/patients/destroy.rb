module Patients
  class Destroy
    include Interactor

    def call
      begin
        patient = Patient.find(context.patient_id)
      rescue ActiveRecord::RecordNotFound
        context.fail! message: {error: 'Пациент не найден. Удаление не завершено'}
      end

      patient.update_attribute :presence_state, Patient.presence_states[:removed]

      if patient.valid?
        context.message = {success: 'Пациент успешно удален'}
      end
    end
  end
end