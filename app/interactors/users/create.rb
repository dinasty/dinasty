module Users
  class Create
    include Interactor

    def call
      result = Patients::CreateUserForPatient.call(user_params: context.user_params)

      if result.success?
        user = result.resource
        user.add_role :patient
        patient = user.create_patient(context.patient_params)
        patient.create_personal_card if patient.valid?
        appointment = patient.appointments.create(context.appointment_params) if patient.valid?
      end

      if appointment.valid?
        context.resource = appointment
        context.message = {success: 'Новый клиент и первичный осмотр успешно созданы'}
      end
    end
  end
end