module Search
  class ByTypes
    include Interactor
    include Concerns::Search::Queries

    def call
      user = context.current_user
      type = context.type
      query = context.query
      short_search_query = "%#{query.mb_chars.downcase}%"

      if type == 'doctors'
        users = users query
        doctors = Doctor.where(["phone || lower(position) LIKE ?", "%#{query.downcase}%"])
        all_results = Doctor.where('user_id IN (?) OR id IN (?)', users.ids, doctors.ids)

      elsif type == 'age_types'
        all_results = AgeType.where([ "lower(title) || lower_age || upper_age LIKE ? OR lower(title) LIKE ?",
                                      "%#{query.mb_chars.downcase.delete(' ').to_s}%",
                                       "%#{query.mb_chars.downcase.to_s}%" ])

      elsif type == 'appointments'
        appointments = (user.has_role? :doctor) ? user.doctor.appointments : Appointment
        users = users query
        user_doctors = user_doctors query, users
        user_patients = user_patients query, users, patients: nil
        all_results = appointments.where('doctor_id IN (?) OR patient_id IN (?)', user_doctors.ids, user_patients.ids)

      elsif type == 'articles'
        users = users query
        user_doctors = user_doctors query, users
        all_results = Article.where(["lower(title) || lower(content) LIKE ? OR doctor_id IN (?)",
                                     "%#{query.mb_chars.downcase}%", user_doctors.ids])

      elsif type == 'conditional_appointments'
        cond_appointments = (user.has_role? :doctor) ? user.doctor.conditional_appointments : ConditionalAppointment
        users = users query
        user_doctors = user_doctors query, users
        all_results = cond_appointments.where([
               "lower(name) || lower(surname) || lower(surname) || lower(name) || lower(email) ||
               'lower(name) + lower(surname)' || 'lower(surname) + lower(name)' || phone LIKE ? OR doctor_id IN (?)",
               "%#{query.mb_chars.downcase.delete(' ').to_s}%", user_doctors.ids
            ])

      elsif type == 'patients'
        users = users query
        if user.doctor?
          patient_ids = user.doctor.treatments.map(&:patient_id).uniq
          all_patients = Patient.where(id: patient_ids)
          all_results = user_patients query, users, patients: all_patients

        else
          all_results = user_patients query, users, patients: nil
        end

      elsif type == 'price_list_categories'
        all_results = PriceListCategory.where(["lower(title) LIKE ? OR lower(description) LIKE ?",
                                     short_search_query, short_search_query])

      elsif type == 'procedures'
        categories = PriceListCategory.where(["lower(title) LIKE ? OR lower(description) LIKE ?",
                                              short_search_query, short_search_query])
        all_results = Procedure.where(["lower(title) LIKE ? OR category_id IN (?)",
                                       "%#{query.mb_chars.downcase}%", categories.ids])

      elsif type == 'videos'
        all_results = Video.where(["lower(title) || lower(link) LIKE ?", "%#{query.mb_chars.downcase}%"])
      end

      context.resources = all_results
    end
  end
end