class Article < ActiveRecord::Base
  belongs_to :doctor

  validates_presence_of :title, :doctor, :content

  mount_uploader :image, ImageUploader

  def self.article_author current_user
    if current_user.doctor?
      doctor = current_user.doctor
      result = {(doctor.name + ' ' + doctor.surname) => doctor.id}

    else
      doctors = Doctor.includes(:user)
      array = doctors.map { |doctor| {(doctor.name + ' ' + doctor.surname) => doctor.id} }
      result = array != [] ? array.inject(:merge) : []
    end

    result
  end

end
