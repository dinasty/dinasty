class PriceListCategory < ActiveRecord::Base
  has_many :procedures, foreign_key: :category_id

  validates_presence_of :title
end
