class TreatmentDetail < ActiveRecord::Base
  belongs_to :treatment
  belongs_to :procedure

  delegate :doctor, :treatment_date, :start_time, :end_time, to: :treatment

  def self.hosted_treatments patient, treatment_date: nil
    if treatment_date
      patient.treatment_details.joins(:treatment).where(
          treatments: {
              state: Treatment.states[:hosted],
              treatment_date: treatment_date
          }
      ).includes(:procedure)
    else
      patient.treatment_details.joins(:treatment).where(
          treatments: { state: Treatment.states[:hosted] }
      )
    end
  end

  def detail_time
    "#{self.start_time.strftime('%H:%M')}" + '-' + "#{self.end_time.strftime('%H:%M')}"
  end

end
