class Delivery < ActiveRecord::Base

  validates_presence_of :message

  def send_delivery_sms phones
    http = Net::HTTP.new('smspro.nikita.kg')
    resp = http.post('/api/message', delivery_message(self, phones))
    Hash.from_xml(resp.body)['response']['status']
  end

  private

  def delivery_message msg, phones
    build = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.message {
        xml.login ENV["SMS_NIKITA_LOGIN"]
        xml.pwd ENV["SMS_NIKITA_PASS"]
        xml.id msg.id.to_s.rjust(7, 'R00000')
        xml.sender "dinastiya"
        xml.text_ msg.message
        # xml.time self.send_time.strftime("%Y%m%d%H%M%S")
        xml.phones {
          phones.each do |phone|
            xml.phone phone
          end
        }
        # xml.test '1'
      }
    end
    build.to_xml
  end

end
