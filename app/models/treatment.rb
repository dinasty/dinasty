class Treatment < ActiveRecord::Base
    belongs_to :patient
    belongs_to :doctor
    belongs_to :procedure
    belongs_to :age_type
    has_many :treatment_details, dependent: :destroy
    has_many :messages

    enum state: [:unstarted, :in_process, :hosted, :postponed, :canceled]
    enum presence_state: [:present, :removed]

    def self.teeth_numeration
        top_right = (11..18).to_a
        top_left = (21..28).to_a
        bottom_left = (31..38).to_a
        bottom_right = (41..48).to_a

        top_right + top_left + bottom_left + bottom_right
    end

    def treatment_time
        "#{start_time.strftime('%H:%M')} " + '-' + " #{end_time.strftime('%H:%M')}"
    end

    def treatment_start
        "#{treatment_date.strftime('%Y-%m-%d')} #{start_time.strftime('%H:%M')}".to_datetime
    end

    def treatment_end
        "#{treatment_date.strftime('%Y-%m-%d')} #{end_time.strftime('%H:%M')}".to_datetime
    end
end
