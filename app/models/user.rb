class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  rolify

  # :validatable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  has_one :doctor
  has_one :patient
  has_and_belongs_to_many :roles, :join_table => :users_roles

  validates_presence_of :name, :surname
  validates_uniqueness_of :email, allow_blank: true

  accepts_nested_attributes_for :patient

  enum user_type: [:patient, :doctor]

  def self.generate_password
    password_length = 10
    Devise.friendly_token.first(password_length)
  end

  def fullname
    "#{self.surname} " + "#{self.name}"
  end

  def admin?
    self.has_role? :admin
  end

  def doctor?
    self.has_role? :doctor
  end
end
