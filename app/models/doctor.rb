class Doctor < ActiveRecord::Base
  belongs_to :user
  has_many :appointments
  has_many :conditional_appointments
  has_many :articles
  has_many :patients, through: :appointments
  has_many :treatments

  validates_presence_of :phone

  delegate :name, :surname, :email, :avatar, :default_image, to: :user

  enum presence_state: [:working, :removed]
  enum work_status: [:present, :on_holiday, :on_the_sick, :fired]

  accepts_nested_attributes_for :user

  DEFAULT_IMAGE = 'doctor_1.jpg'

  def fullname
    "#{self.surname} " + "#{self.name}"
  end

  def today_treatments
    self.treatments.where(treatment_date: Date.today)
  end

  def this_week_treatments
    today = Date.today
    week_beginning = today.at_beginning_of_week
    week_ending = today.at_end_of_week
    self.treatments.where('treatment_date >= ? and treatment_date <= ?', week_beginning, week_ending).
        order(treatment_date: :asc)
  end

  def self.selecting_doctors
    doctors = Doctor.includes(:user)
    array = doctors.map {|doctor| {(doctor.name + ' ' + doctor.surname) => doctor.id }}
    result = array != [] ? array.inject(:merge) : []

    result
  end
end
