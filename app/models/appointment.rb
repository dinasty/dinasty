class Appointment < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :patient

  enum status: [:not_approved, :approved, :postponed, :canceled]

  validates_presence_of :doctor_id, :patient_id, :appointment_date, :start_time

  def self.appointment_participants(member)
    doctors = Doctor.includes(:user)
    patients = Patient.includes(:user)

    if member.kind_of? Doctor
      array = doctors.map {|doctor| {(doctor.name + ' ' + doctor.surname) => doctor.id }}
    elsif member.kind_of? Patient
      array = patients.map {|patient| {(patient.name + ' ' + patient.surname) => patient.id }}
    end

    if array != []
      array.inject(:merge)
    else
      []
    end
  end

end
