class ProcedureAgeType < ActiveRecord::Base
  belongs_to :procedure
  belongs_to :age_type
end
