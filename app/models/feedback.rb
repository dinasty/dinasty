class Feedback < ActiveRecord::Base

  validates_presence_of :name, :message

  def update_status!
    if self.show
      self.update(show: false)
    else
      self.update(show: true)
    end
  end
end
