class Message < ActiveRecord::Base
  belongs_to :treatment

  enum status: [:created, :sent, :delivered]

  def send_custom_sms
    http = Net::HTTP.new('smspro.nikita.kg')
    resp = http.post('/api/message', custom_message(self))
    Hash.from_xml(resp.body)['response']['status']
  end

  private

  def custom_message msg
    build = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.message {
        xml.login ENV["SMS_NIKITA_LOGIN"]
        xml.pwd ENV["SMS_NIKITA_PASS"]
        xml.id msg.id
        xml.sender "dinastiya"
        xml.text_ msg.text
        # xml.time self.send_time.strftime("%Y%m%d%H%M%S")
        xml.phones {
          xml.phone msg.phone
        }
        xml.test '1'
      }
    end
    build.to_xml
  end
end
