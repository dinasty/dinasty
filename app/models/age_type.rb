class AgeType < ActiveRecord::Base
  has_many :procedure_age_types
  has_many :procedures, through: :procedure_age_types
  has_many :treatments

  validates_presence_of :title, :upper_age, :lower_age
end
