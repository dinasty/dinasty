class ConditionalAppointment < ActiveRecord::Base
  belongs_to :doctor

  enum appointment_status: [:waiting, :canceled, :processed]
  validates_presence_of :name, :surname, :phone

  def time_diff start_time
    seconds_diff = (start_time - self.created_at).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
  end

  def fullname
    "#{self.surname} " + "#{self.name}"
  end
end
