class Procedure < ActiveRecord::Base
  belongs_to :category, class_name: 'PriceListCategory', foreign_key: :category_id
  has_many :procedure_age_types
  has_many :age_types, through: :procedure_age_types
  has_many :treatment_details

  validates_presence_of :title, :category

  accepts_nested_attributes_for :procedure_age_types
  accepts_nested_attributes_for :age_types
end
