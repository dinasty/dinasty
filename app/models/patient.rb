class Patient < ActiveRecord::Base
  belongs_to :user
  has_one :personal_card
  has_many :appointments
  has_many :doctors, through: :appointments
  has_many :treatments
  has_many :treatment_details, through: :treatments

  validates_presence_of :phone

  enum status: [:active, :inactive]
  enum presence_state: [:present, :removed]

  delegate :name, :surname, :email, :avatar, :default_image, to: :user

  accepts_nested_attributes_for :appointments
  accepts_nested_attributes_for :user

  DEFAULT_AVATAR = 'patient_default.jpg'

  def fullname
    "#{self.surname} " + "#{self.name}"
  end
end