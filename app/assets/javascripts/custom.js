$(function() {
    initPageForRemoveTreatmentFields();
});

$(window).bind('page:change', function() {
    initPageForRemoveTreatmentFields();
});

function initPageForRemoveTreatmentFields() {
    $(document).ready(function () {
        var $redButton = $('[data="red_button"]');
        $redButton.on('click', function () {
            $(this).parents('.fields').remove();
        });
    });
}
