$(function() {
    initPageforSemantic();
});

$(window).bind('page:change', function() {
    initPageforSemantic();
});

function initPageforSemantic() {
    $(document).ready(function () {
        $('.ui.checkbox').checkbox();
        $('.ui.dropdown').dropdown();
        $('.ui.accordion').accordion();
        $('.secondary.menu .item').tab();
        $('.tabular.menu .item').tab();
    });
}
