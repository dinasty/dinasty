$(document).ready(function () {
    var $treatmentForm = $('.edit_treatment');
    var $dateSelect = $treatmentForm.find('#determined_date');
    var $doctorSelect = $treatmentForm.find('#determined_doctor');
    var $doctorTimeSelect = $treatmentForm.find('#determined_doctor_free_time option');
    var $TimeSelectWrapper = $treatmentForm.find('#treatment_start_time');
    // var $timeRange =  $treatmentForm.find('#determined_time_range');

    $(document).on('change', ($doctorSelect, $dateSelect), function () {
        var selected_date = $dateSelect.val();
        var selected_doctor = $doctorSelect.val();
        if (selected_doctor && selected_date) {
            $.ajax({
                type: 'GET',
                url: '/admin/doctors/determine_doctor_time',
                data: {
                    "doctor_id": selected_doctor,
                    "date": selected_date
                },

                success: function (data) {
                    $doctorTimeSelect.each(function () {
                        var $that = $(this);
                        var time = parseInt($that.val());
                        if ($.inArray(time, data) != -1) {
                            $TimeSelectWrapper.find('[data-value=' + $that.val() + ']').addClass('disabled')
                        } else {
                            $TimeSelectWrapper.find('[data-value=' + $that.val() + ']').removeClass('disabled')
                        }
                    });
                }
            });
        }
    });
});
