$(function() {
    initPage();
});

$(window).bind('page:change', function() {
    initPage();
});

function initPage() {

    $.fn.datepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        daysShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", 'Сентябрь', "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Нояб", "Дек"],
        clear: "Очистить дату",
        titleFormat: "MM yyyy"
    };

    $('[data-behaviour~=datepicker]').datepicker({
        container: '#datepicker_identifier',
        format: "dd/mm/yyyy",
        weekStart: 1,
        clearBtn: true,
        language: "ru",
        daysOfWeekDisabled: "0",
        startDate: '-0d',
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });

    $('[data-provide~=datepicker]').datepicker({
        container: '#second_datepicker_identifier_on_page',
        format: "dd/mm/yyyy",
        weekStart: 1,
        clearBtn: true,
        language: "ru",
        daysOfWeekDisabled: "0",
        startDate: '-0d',
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });
}
