$(function() {
    initPageforFlash();
});

$(window).bind('page:change', function() {
    initPageforFlash();
});

function initPageforFlash() {
    $(document).ready(function () {
        alertify.set('notifier', 'position', 'top-right');
        alertify.set('notifier', 'delay', 5);
    });
}
