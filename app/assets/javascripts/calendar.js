$(document).ready(function () {
    $('#calendar').fullCalendar({
        eventSources: [
            {
                url: "/admin/calendar/events.json"
            }
        ],
        editable: false,
        nowIndicator: true,
        axisFormat: 'HH:mm',
        timeFormat: 'H:mm',
        minTime: '08:00:00',
        maxTime: '19:00:00',
        weekends: 'Sunday',
        defaultView: "agendaWeek",
        selectable: true,
        selectHelper: true,
        firstDay: 1,
        displayEventEnd: true,
        eventColor: 'lightgray',
        eventTextColor: 'black',
        height: 600,

        header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
        },

        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        dayNamesShort: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],

        buttonText: {
            today: "Сегодня",
            month: "Месяц",
            week: "Неделя",
            day: "День"
        }
    });
    var $doctorSelect = $('#filter_doctor');
    $doctorSelect.on('change', function () {
        doctor_id = $(this).find(":selected").val();

        $.ajax({
            type: 'GET',
            url: '/admin/calendar/events',
            data: {
                "doctor_id": doctor_id
            },
            success: function (data) {
                $('#calendar').fullCalendar('removeEvents');
                $("#calendar").fullCalendar("addEventSource", data)
            }
        });

    });
});