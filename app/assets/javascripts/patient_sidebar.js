$(function() {
    initPatientSidebarPage();
});

$(window).bind('page:change', function() {
    initPatientSidebarPage();
});

function initPatientSidebarPage() {
    $(document).ready(function () {
        $("#sidebar_button").on("click", function () {
            $('.ui.sidebar')
                .sidebar('setting', 'transition', 'overlay')
                .sidebar('toggle')
        });
    });
}