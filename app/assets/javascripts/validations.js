$(function() {
    initValidationsPage();
});

$(window).bind('page:change', function() {
    initValidationsPage();
});

function initValidationsPage() {
    $('.create_video').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название для видео файла' }]
            },

            link: { identifier: 'link',
                rules: [{ type: 'url', prompt: 'Введите ссылку для проигрывания файла' }]
            },
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_video').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название для видео файла' }]
            },

            link: { identifier: 'link',
                rules: [{ type: 'url', prompt: 'Введите ссылку для проигрывания файла' }]
            },
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_article').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название статьи' }]
            },

            content: { identifier: 'content',
                rules: [{ type: 'empty', prompt: 'Содержание статьи не должно быть пустым' }]
            },

            author : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_article').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название статьи' }]
            },

            content: { identifier: 'content',
                rules: [{ type: 'empty', prompt: 'Содержание статьи не должно быть пустым' }]
            },

            author : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_age_type').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите наименование категории' }]
            },

            lower_age : { identifier: 'lower_age',
                rules: [
                    { type: 'empty', prompt: 'Введите нижнюю границу возраста' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            },

            upper_age : { identifier: 'upper_age',
                rules: [
                    { type: 'empty', prompt: 'Введите верхнюю границу возраста' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_age_type').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите наименование категории' }]
            },

            lower_age : { identifier: 'lower_age',
                rules: [
                    { type: 'empty', prompt: 'Введите нижнюю границу возраста' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            },

            upper_age : { identifier: 'upper_age',
                rules: [
                    { type: 'empty', prompt: 'Введите верхнюю границу возраста' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_price_list_category').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название категории' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_price_list_category').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите название категории' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_procedure').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите наименование процедуры' }]
            },

            category : { identifier: 'category_id',
                rules: [{ type: 'empty', prompt: 'Выберите категорию из списка' }]
            },

            age_type : { identifier: 'age_type',
                rules: [{ type: 'empty', prompt: 'Необходимо указать хотя бы одну возрастную категорию' }]
            },

            price : { identifier: 'price',
                rules: [{ type: 'empty', prompt: 'Укажите цену на процедуру' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_procedure').form({
        fields: {
            title: { identifier: 'title',
                rules: [{ type: 'empty', prompt: 'Введите наименование процедуры' }]
            },

            category : { identifier: 'category_id',
                rules: [{ type: 'empty', prompt: 'Выберите категорию из списка' }]
            },

            age_type_id : { identifier: 'age_type',
                rules: [{ type: 'empty', prompt: 'Выберите возрастную категорию' }]
            },

            price : { identifier: 'price',
                rules: [{ type: 'empty', prompt: 'Укажите цену на процедуру' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_doctor').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя доктора' }]
            },

            surname: { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию доктора' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_doctor').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя доктора' }]
            },

            surname: { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию доктора' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_patient').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя пациента' }]
            },

            surname: { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию пациента' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            },

            age : { identifier: 'age',
                rules: [
                    { type: 'empty', prompt: 'Введите возраст пациента' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_patient').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя пациента' }]
            },

            surname: { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию пациента' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            },

            age : { identifier: 'age',
                rules: [
                    { type: 'empty', prompt: 'Введите возраст пациента' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_appointment_for_existing_patient').form({
        fields: {
            doctor : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            },

            patient : { identifier: 'patient_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из пациентов' }]
            },


            date : { identifier: 'appointment_date',
                rules: [{ type: 'empty', prompt: 'Заполните дату ' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_appointment_for_new_patient').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя пациента' }]
            },

            surname: { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию пациента' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            },

            age : { identifier: 'age',
                rules: [
                    { type: 'empty', prompt: 'Введите возраст пациента' },
                    { type: 'integer', prompt: 'Необходимо ввести цифру' }
                ]
            },

            doctor : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            },

            date : { identifier: 'appointment_date',
                rules: [{ type: 'empty', prompt: 'Заполните дату ' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.create_conditional_appointment').form({
        fields: {
            name : { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите Ваше имя' }]
            },

            surname : { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите Вашу фамилию' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_conditional_appointment').form({
        fields: {
            name : { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите имя пациента' }]
            },

            surname : { identifier: 'surname',
                rules: [{ type: 'empty', prompt: 'Введите фамилию пациента' }]
            },

            phone : { identifier: 'phone',
                rules: [
                    { type: 'empty', prompt: 'Введите телефон' },
                    { type: 'integer', prompt: 'Номер телефона не может содержать символы и пробелы' },
                    { type: 'exactLength[12]', prompt: 'номер телефона должен выглядеть так 996XXX123456' }
                ]
            },

            //age : { identifier: 'age',
            //    rules: [
            //        { type: 'empty', prompt: 'Введите возраст пациента' },
            //        { type: 'integer', prompt: 'Необходимо ввести цифру' }
            //    ]
            //},

            doctor : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            },

            date : { identifier: 'appointment_date',
                rules: [{ type: 'empty', prompt: 'Заполните дату ' }]
            }
        },
        inline : true,
        on     : 'blur'
    });

    $('.edit_treatment').form({
        fields: {
            doctor : { identifier: 'doctor_id',
                rules: [{ type: 'empty', prompt: 'Выберите одного из докторов' }]
            },

            date : { identifier: 'treatment_date',
                rules: [{ type: 'empty', prompt: 'Заполните дату ' }]
            },

            // tooth : { identifier: 'therapy[][tooth]',
            //    rules: [{ type: 'empty', prompt: 'Выберите зуб для лечения' }]
            // },
            //
            procedure : { identifier: 'procedure',
               rules: [{ type: 'empty', prompt: 'Выберите процедуры' }]
            }
        },
        inline : true,
        on     : 'blur'
    });
    
    $('.create_feedback').form({
        fields: {
            name: { identifier: 'name',
                rules: [{ type: 'empty', prompt: 'Введите ваше имя' }]
            },
    
            message: { identifier: 'message',
                rules: [{ type: 'empty', prompt: 'Содержание сообщения не должно быть пустым' }]
            }
    
            // phone : { identifier: 'phone',
            //     rules: [{ type: 'empty', prompt: 'Введите Ваш номер телефона +996 XXX YYSSZZ' }]
            // }
        },
        inline : true,
        on     : 'blur'
    });
}
