class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.title}"
  end

  def content_type_whitelist
    /image\//
  end

  CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/

  version :tiny do
    process resize_to_fill: [200,200]
  end

  version :thumb do
    process resize_to_fill: [370,212]
  end

  version :large do
    process resize_to_fill: [640,480]
  end

end