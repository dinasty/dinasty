class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.name} #{model.surname}"
  end

  def content_type_whitelist
    /image\//
  end

  CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/

  version :tiny do
    process resize_to_fill: [40,40]
  end

  version :large do
    process resize_to_fill: [225,250]
  end

end