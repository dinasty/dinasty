class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotUnique, with: :record_not_unique

  before_action :configure_permitted_parameters, if: :devise_controller?

  private

  def configure_permitted_parameters
    permitted_attributes = [:name, :surname, :age, :gender, :phone]
    devise_parameter_sanitizer.permit :sign_up, keys: permitted_attributes
  end

  def access_denied(exception)
    flash[:danger] = exception.message
    redirect_to root_url
  end

  def record_not_unique exception
   redirect_to :back
   flash[:error] = exception.message
  end

end
