class LandingController < ApplicationController

  layout 'landing'

  def index
    @articles = Article.order(updated_at: :desc).take(3)
    @appointment = ConditionalAppointment.new
  end
end
