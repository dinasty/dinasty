class DeviseOverriden::RegistrationController < Devise::RegistrationsController

  def create
    super
    patient_params = permitted_params[:user][:patient_attributes]

    if resource.valid?
      resource.create_patient(patient_params.merge(user_id: resource.id))
    end
  end

  private

  def permitted_params
    params.permit user: [:name, :surname, :email, :password, :password_confirmation,
                  patient_attributes: [:phone, :age, :gender]]
  end
end