class DeviseOverriden::SessionController < Devise::SessionsController

  def new
    super
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    yield resource if block_given?

    if resource.has_role? :admin
      respond_with resource, location: conditional_appointments_path
      success_login

    elsif resource.has_role? :doctor
      respond_with resource, location: appointments_path
      success_login

    else
      respond_with resource, location: root_path
      flash[:error] = 'Вход разрешен только зарегистрированным пользователям'
    end
  end

  private

    def success_login
      flash[:success] = 'Вход выполнен успешно'
    end

end