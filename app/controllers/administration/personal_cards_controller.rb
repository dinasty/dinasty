module Administration
  class PersonalCardsController < AdminController

    def index
      patient = Patient.find params[:patient_id]
      @personal_card = patient.personal_card
      @treatments = patient.treatments
    end

    def edit
      @personal_card = PersonalCard.find(params[:id])
    end

    def update
      result = PersonalCards::Update.call(
        personal_card_id: params[:id],
        personal_card_params: permitted_params
      )

      result_redirects(result, patient_personal_cards_path(result.resource.patient), result.message)
    end

    def history
      patient = Patient.find params[:id]
      personal_card = patient.personal_card
      output = CardHistory.new(patient, personal_card)
      send_data output.render, :type => 'application/pdf', :filename => "personal_card.pdf", :disposition => "inline"
    end

    private

      def permitted_params
        params.require(:personal_card).permit :disease, :allergy, :medicine, :bleeding,
                      :pregnancy, :gestational_age, :patient_id
      end

  end
end