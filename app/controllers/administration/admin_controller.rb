class Administration::AdminController < ApplicationController
  before_action :authenticate_user

  private

  def authenticate_user
    if current_user.present?
      unless current_user.has_role? :admin or current_user.has_role? :doctor
        redirect_to_root
      end

    elsif current_user.nil?
      redirect_to_root
    end
  end

  def result_redirects(result, path, message)
    if result and message and path
      redirect_to path
      show_result_message message

    elsif message.nil?
      redirect_to path

    elsif path.nil? and message.present?
      redirect_to :back
      show_result_message message
    end
  end

  def redirect_to_root
    redirect_to root_path
    flash[:error] = 'У Вас нет доступа к данной странице'
  end

  def show_result_message message
    message[:success].present? ? flash[:success] = message[:success] : flash[:error] = message[:error]
  end

end