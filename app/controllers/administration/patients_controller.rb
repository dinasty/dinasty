module Administration
  class PatientsController < AdminController

    def index
      result = Patients::List.call(
          sort: params[:sort],
          direction: params[:direction],
          page: params[:page],
          current_user: current_user
      )

      @patients = result.resources
      @patient = Patient.new
    end

    def show
      @patient = Patient.find(params[:id])

      result = Treatments::List.call(
          sort: params[:sort],
          direction: params[:direction],
          page: params[:page],
          patient: @patient,
          current_user: current_user
      )

      @treatments = result.resources
    end

    def new
      @patient = Patient.new
    end

    def edit
      @patient = Patient.find(params[:id])
    end

    def create
      result = Patients::Create.call(
        user_params: user_params,
        patient_params: patient_params
      )

      result_redirects(result, patients_path, result.message)
    end

    def update
      result = Patients::Update.call(
          patient_id: params[:id],
          patient_params: patient_params,
          user_params: user_params
      )

      result_redirects(result, patients_path, result.message)
    end

    def destroy
      result = Patients::Destroy.call(patient_id: params[:id])
      result_redirects(result, patients_path, result.message)
    end

    def change_status
      result = Patients::ChangeStatus.call(patient_id: params[:id])
      result_redirects(result, patients_path, result.message)
    end

    private

    def permitted_params
      params.permit :phone, :age, :name, :surname,
                    patient: [:gender, user_attributes: [:email, :avatar, :avatar_cache]]
    end

    def patient_params
      permitted_params[:patient].except(:user_attributes).merge(
        'phone' => permitted_params[:phone],
        'age' => permitted_params[:age]
      )
    end

    def user_params
      permitted_params[:patient][:user_attributes].merge(
        'name' => permitted_params[:name],
        'surname' => permitted_params[:surname]
      )
    end

  end
end