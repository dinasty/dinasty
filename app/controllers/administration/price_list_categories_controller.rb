module Administration
  class PriceListCategoriesController < AdminController

    def index
      result = PriceListCategories::List.call(
        sort: params[:sort],
        direction: params[:direction]
      )

      @categories = result.resources
      @category = PriceListCategory.new
    end

    def create
      result = PriceListCategories::Create.call(permitted_params: permitted_params)
      result_redirects(result, price_list_categories_path, result.message)
    end

    def update
      result = PriceListCategories::Update.call(
        category_id: params[:id],
        permitted_params: permitted_params
      )

      result_redirects(result, price_list_categories_path, result.message)
    end

    def destroy
      result = PriceListCategories::Destroy.call(category_id: params[:id])
      result_redirects(result, price_list_categories_path, result.message)
    end

    private

      def permitted_params
        params.permit :title, :description
      end
  end
end
