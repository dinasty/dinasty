module Administration
  class ConditionalAppointmentsController < AdminController
    before_action :authenticate_user, only: [:index, :edit, :update, :destroy]

    def index
      result = ConditionalAppointments::List.call(
        sort: params[:sort],
        direction: params[:direction],
        page: params[:page],
        current_user: current_user
      )

      @conditional_apps = result.resources
      @appointment = Appointment.new
      @user = User.new
    end

    def new
      @conditional_app = ConditionalAppointment.new
    end

    def edit
      @conditional_app = ConditionalAppointment.find(params[:id])
    end

    def create
      result = ConditionalAppointments::Create.call(permitted_params: conditional_appointment_params)
      result_redirects(result, root_path, result.message)
    end

    def update
      result = ConditionalAppointments::Update.call(
          conditional_app_id: params[:id],
          user_params: user_params,
          patient_params: patient_params,
          appointment_params: appointment_params
      )

      result_redirects(result, appointments_path, result.message)
    end

    def destroy
      result = ConditionalAppointments::Destroy.call(conditional_app_id: params[:id])
      result_redirects(result, conditional_appointments_path, result.message)
    end

    def change_status
      result = ConditionalAppointments::ChangeStatus.call(
        appointment_id: params[:id],
        status: params[:status]
      )

      result_redirects(result, conditional_appointments_path, result.message)
    end

    private

    def permitted_params
      params.permit :name, :surname, :phone, :age, :doctor_id, :appointment_date,
                    :utf8, :authenticity_token, :commit, :action,
                    conditional_appointment: [:start_time, :email]
    end

    def user_params
      {
        'email' => permitted_params[:conditional_appointment][:email],
        'name' => permitted_params[:name],
        'surname' => permitted_params[:surname]
      }
    end

    def patient_params
      {
        'phone' => permitted_params[:phone],
        'age' => permitted_params[:age]
      }
    end

    def appointment_params
      permitted_params[:conditional_appointment].except(:email).
          merge(
              'doctor_id' => permitted_params[:doctor_id],
              'appointment_date' => permitted_params[:appointment_date]
          )
    end

    def conditional_appointment_params
      user_params.merge('phone' => permitted_params[:phone])
    end

  end
end
