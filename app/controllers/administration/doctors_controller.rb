module Administration
  class DoctorsController < AdminController

    def index
      result = Doctors::List.call(
          sort: params[:sort],
          direction: params[:direction],
          page: params[:page]
      )

      @doctors = result.resources
      @doctor = Doctor.new
    end

    def new
      @doctor = Doctor.new
    end

    def edit
      @doctor = Doctor.find(params[:id])
    end

    def show
      @doctor = Doctor.find(params[:id])
      @today_appointments = today_appointments(@doctor)
    end

    def create
      result = Doctors::Create.call(
        user_params: user_params,
        doctor_params: doctor_params
      )

      result_redirects(result, doctors_path, result.message)
    end

    def update
      result = Doctors::Update.call(
        doctor_id: params[:id],
        user_params: user_params,
        doctor_params: doctor_params
      )

      result_redirects(result, doctors_path, result.message)
    end

    def destroy
      result = Doctors::Destroy.call(doctor_id: params[:id])
      result_redirects(result, doctors_path, result.message)
    end

    def manage_status
      result = Doctors::ManageStatus.call(
        doctor_id: params[:id],
        work_status: params[:work_status]
      )

      result_redirects(result, doctors_path, result.message)
    end

    def determine_doctor_time
      result = Treatments::DetermineDoctorTime.call(
        doctor_id: params[:doctor_id],
        treatment_date: params[:date]
      )
       
      render json: result.resource
    end

    private

      def permitted_params
        params.permit :name, :surname, :phone,
                      doctor: [:position, :work_history, :work_experience, :work_status,
                               user_attributes: [:email, :avatar, :avatar_cache]]
      end

      def doctor_params
        permitted_params[:doctor].except(:user_attributes).
            merge('phone' => permitted_params[:phone])
      end

      def user_params
        permitted_params[:doctor][:user_attributes].
            merge(
                'name' => permitted_params[:name],
                'surname' => permitted_params[:surname]
            )
      end

      def today_appointments(doctor)
        doctor.appointments.where(appointment_date: :today)
      end

  end
end