module Administration
  class AppointmentsController < AdminController

    def index
      result = Appointments::List.call(
        sort: params[:sort],
        direction: params[:direction],
        page: params[:page],
        current_user: current_user
      )

      @appointments = result.resources
      @appointment = Appointment.new
      @user = User.new
    end
    def edit
      @appointment = Appointment.find(params[:id])
    end

    def new
      @appointment = Appointment.new
    end

    def create
      result = Appointments::Create.call(permitted_params: permitted_params)
      result_redirects(result, appointments_path, result.message)
    end

    def update
      result = Appointments::Update.call(
        appointment_id: params[:id],
        permitted_params: permitted_params
      )

      result_redirects(result, appointments_path, result.message)
    end

    def destroy
      result = Appointments::Destroy.call(appointment_id: params[:id])
      result_redirects(result, appointments_path, result.message)
    end

    def change_status
      result = Appointments::ChangeStatus.call(
        appointment_id: params[:id],
        status: params[:status]
      )

      result_redirects(result, appointments_path, result.message)
    end

    private

    def app_params
      params.permit :doctor_id, :patient_id, :appointment_date,
                    :utf8, :authenticity_token, :commit,
                    appointment: [:start_time, :status]
    end

    def permitted_params
      app_params[:appointment].merge(
        doctor_id: params[:doctor_id],
        patient_id: params[:patient_id],
        appointment_date: params[:appointment_date]
      )
    end

  end
end
