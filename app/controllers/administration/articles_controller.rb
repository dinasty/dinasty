module Administration
  class ArticlesController < AdminController
    before_action :authenticate_user, except: :show

    def index
      all_articles = current_user.doctor? ? current_user.doctor.articles : Article
      @articles = all_articles.order(created_at: :desc)
      @article = Article.new
    end

    def create
      @article = Article.new article_params
      if @article.save
        redirect_to articles_path
        flash[:success] = 'Статья успешно создана'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при создании статьи'
      end
    end

    def show
      @article = Article.find params[:id]
    end

    def update
      @article = Article.find params[:id]
      if @article.update! article_params
        redirect_to articles_path
        flash[:success] = 'Статья успешно обновлена'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при обновлении статьи'
      end
    end

    def destroy
      article = Article.find params[:id]
      if article.destroy
        redirect_to articles_path
        flash[:success] = 'Статья успешно удалена'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при удалении статьи'
      end
    end

    private

    def article_params
      params.permit :title, :content, :doctor_id, :image
    end
  end
end
