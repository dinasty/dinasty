module Administration
  class AgeTypesController < AdminController

    def index
      @age_types = AgeType.all.order(title: :asc) if AgeType.any?
      @age_type = AgeType.new
    end

    def create
      result = AgeTypes::Create.call(permitted_params: permitted_params)
      result_redirects(result, age_types_path, result.message)
    end
    def new

    end

    def update
      result = AgeTypes::Update.call(
        age_type_id: params[:id],
        permitted_params: permitted_params
      )

      result_redirects(result, age_types_path, result.message)
    end

    def destroy
      result = AgeTypes::Destroy.call(age_type_id: params[:id])
      result_redirects(result, age_types_path, result.message)
    end

    private

      def permitted_params
        params.permit :title, :lower_age, :upper_age
      end

  end
end
