module Administration
  class SearchesController < AdminController

    def by_types
      search_results = Search::ByTypes.call(
        query: params[:search_query],
        type: params[:search_type],
        current_user: current_user
      )

      @type = search_results.type
      @results = search_results.resources

      if @type == 'doctors'
        @doctors = @results.includes(:user).order(work_status: :asc).page(params[:page])
        
      elsif @type == 'age_types'
        @age_types = @results.order(title: :asc).page(params[:page])

      elsif @type == 'appointments'
        @appointments = @results.order(created_at: :desc).page(params[:page])

      elsif @type == 'articles'
        @articles = @results.order(created_at: :desc).page(params[:page])

      elsif @type == 'conditional_appointments'
        @conditional_apps = @results.order(appointment_status: :asc, created_at: :desc).page(params[:page])

      elsif @type == 'patients'
        @patients = @results.includes(:user).order(id: :desc).page(params[:page])

      elsif @type == 'price_list_categories'
        @categories = @results.order(title: :asc).
            includes(:procedures, [procedures: :age_types]).page(params[:page])

      elsif @type == 'procedures'
        @procedures = @results.order(title: :asc).
            includes(:procedure_age_types, :age_types, :category).page(params[:page])

      elsif @type == 'videos'
        @videos = @results.order(created_at: :desc).page
      end
    end

  end
end