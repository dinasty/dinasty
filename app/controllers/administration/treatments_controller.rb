module Administration
  class TreatmentsController < AdminController
    before_action :formatting_therapy, only: :update

    def new
      @treatment = Treatment.new
      @patient_id = params[:patient_id]
    end

    def create
      if formatting_therapy.empty?
        flash[:error] = 'Пожалуйста заполните все поля.'
        redirect_to :back and return
      end
      result = Treatments::Create.call(
          assembled_params: assembled_params,
          therapy: formatting_therapy,
          patient_id: params[:patient_id],
          duration: params[:duration]
      )

      result_redirects(result, patient_path(result.resource), result.message)
    end

    def edit
      @treatment = Treatment.find(params[:id])
    end

    def update
      result = Treatments::Update.call(
          assembled_params: assembled_params,
          therapy: formatting_therapy,
          patient_id: params[:patient_id],
          duration: params[:duration]
      )
      result_redirects(result, patient_path(result.resource), result.message)
    end

    def destroy
      result = Treatments::Destroy.call(treatment_id: params[:id])
      result_redirects(result, patient_path(result.resource), result.message)
    end

    def add_primary_fields
    end

    def invoice
      treatment = Treatment.find(params[:treatment_id])
      output = Invoice.new(treatment)
      send_data output.render, :type => 'application/pdf', :filename => "Invoice.pdf", :disposition => "inline"
    end

    def details
      @treatment = Treatment.find(params[:id])
      @treatment_details = @treatment.treatment_details
      @patient = @treatment.patient
    end

    def manage_treatment_statuses #before and after treatment statuses
      result = Treatments::ManageStatuses.call(
          permitted_params: permitted_params,
          treatment_id: params[:id]
      )

      result_redirects(result, patient_personal_cards_path(result.resource.patient), result.message)
    end

    def manage_state
      result = Treatments::ManageState.call(
          treatment_id: params[:id],
          treatment_state: params[:state]
      )

      result_redirects(result, patient_path(result.resource.id), result.message)
    end

    private

    def formatting_therapy
      if params[:therapy].first['tooth'].present? && params[:therapy].first['procedure'].present?
        therapies = params[:therapy].map { |t| {t[:tooth] => t[:procedure]} }
        therapies
      else
        []
      end
    end

    def permitted_params
      params.permit :treatment_date, :doctor_id, :patient_id, :utf8, :authenticity_token, :commit,
                    treatment: [:start_time, :treatment_id, :complaint, :before_treatment_status, :treatment_process]
    end

    def assembled_params
      if permitted_params[:treatment][:before_treatment_status].present? or
          permitted_params[:treatment][:treatment_process].present?
        permitted_params

      else
        teeth_array = formatting_therapy.map { |element| element.keys }.flatten
        procedures_ids = formatting_therapy.map { |element| element.values }.flatten

        permitted_params[:treatment].merge(
            teeth: teeth_array,
            procedures: procedures_ids,
            'treatment_date' => permitted_params[:treatment_date].to_date,
            'doctor_id' => permitted_params[:doctor_id]
        )
      end
    end

  end
end