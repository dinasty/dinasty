module Administration
  class ProceduresController < AdminController

    def index
      result = Procedures::List.call(
        sort: params[:sort],
        direction: params[:direction],
        page: params[:page]
      )

      @procedures = result.resources
      @procedure = Procedure.new
    end

    def create
      result = Procedures::Create.call(
          permitted_params: permitted_params,
          prices_params: formatting_prices
      )

      result_redirects(result, procedures_path, result.message)
    end

    def update
      result = Procedures::Update.call(
          procedure_id: params[:id],
          permitted_params: permitted_params,
          prices_params: formatting_prices
      )

      result_redirects(result, procedures_path, result.message)
    end

    def destroy
      result = Procedures::Destroy.call(procedure_id: params[:id])
      result_redirects(result, procedures_path, result.message)
    end

    private

    def formatting_prices
      if params[:age_type] and params[:price]
        necessary_price = [[params[:age_type], params[:price]]]
      end

      if params[:prices]
        initial_params = params[:prices].map {|price| price.values}
        prices = initial_params.each {|price| price.delete_if(&:empty?)}

        unnecessary_prices = prices.reject {|array| array.empty?}
      end

      if necessary_price.present? and unnecessary_prices.present?
        necessary_price + unnecessary_prices
      elsif necessary_price.present?
        necessary_price
      elsif unnecessary_prices.present?
        unnecessary_prices
      end
    end

    def permitted_params
      params.permit :title, :category_id
    end

  end
end
