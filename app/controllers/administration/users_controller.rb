module Administration
  class UsersController < AdminController

    def new
      @user = User.new
      @appointment = Appointment.new
    end

    def create
      result = Users::Create.call(
        user_params: user_params,
        patient_params: patient_params,
        appointment_params: appointment_params
      )

      result_redirects(result, appointments_path, result.message)
    end

    private

    def permitted_params
      params.permit :name, :surname, :phone, :age, :doctor_id, :appointment_date,
                    :utf8, :authenticity_token, :commit, :action,
                    user: [
                        :email,
                        appointments: [:start_time],
                        patient_attributes: [:gender]
                    ]
    end

    def user_params
      {
        'email' => permitted_params[:user][:email],
        'name' => permitted_params[:name],
        'surname' => permitted_params[:surname]
      }
    end

    def patient_params
      {
        'phone' => permitted_params[:phone],
        'age' => permitted_params[:age]
      }
    end

    def appointment_params
     permitted_params[:user][:appointments].
         merge(
           'doctor_id' => permitted_params[:doctor_id],
           'appointment_date' => permitted_params[:appointment_date]
         )
    end

  end
end
