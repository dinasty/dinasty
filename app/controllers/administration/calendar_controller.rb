module Administration
  class CalendarController < AdminController

    def index
    end

    def events
      if current_user.admin?
        if params[:doctor_id].present?
          @treatments = Treatment.where(doctor_id: params[:doctor_id])
        else
          @treatments = Treatment.where.not(doctor_id: nil)
        end

      elsif current_user.doctor?
        @treatments = current_user.doctor.treatments
      end

      render json: fetch_events
    end

    private

    def fetch_events
      events = []
      @treatments.each do |treatment|
        events.push({
                        title: treatment.patient.fullname + "\n"+ treatment.doctor.fullname,
                        start: treatment.treatment_start,
                        end: treatment.treatment_end,
                        id: treatment.id,
                        backgroundColor: '#CCB0C9',
                        attribute: 'calendar_event'
                    })
      end

      events
    end
  end
end
