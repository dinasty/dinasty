module Administration
  class VideosController < AdminController

    def index
      @videos = Video.all.order(created_at: :desc)
      @video = Video.new
    end

    def create
      @video = Video.new video_params
      if @video.save
        redirect_to videos_path
        flash[:success] = 'Видео успешно добавлено'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при добавлении видео'
      end
    end

    def update
      @video = Video.find params[:id]
      if @video.update! video_params
        redirect_to videos_path
        flash[:success] = 'Видео успешно обновлено'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при обновлении видео'
      end
    end

    def destroy
      video = Video.find params[:id]
      if video.destroy
        redirect_to videos_path
        flash[:success] = 'Видео успешно удалено'
      else
        redirect_to :back
        flash[:error] = 'Ошибка при удалении видео'
      end
    end

    private

    def video_params
      params.permit(:title, :link)
    end
  end
end
