module Administration
  class FeedbacksController < AdminController
    skip_before_action :authenticate_user, only: :create

    def index
      @feedbacks = Feedback.all
    end

    def create
      @feedback = Feedback.create(feedback_params)
      if @feedback.save
        flash[:success] = "Ваше сообщение успешно отправлено"
        redirect_to root_path
      else
        flash[:error] = "Вы не заполнили обязательные поля"
        redirect_to root_path + '#contacts'
      end

    end

    def update_status
      @feedback = Feedback.find(params[:id])
      @feedback.update_status!

      redirect_to feedbacks_path
      flash[:success] = "Статус успешно обновлен"
    end

    private

    def feedback_params
      params.permit :name, :phone, :email, :message
    end

  end
end
