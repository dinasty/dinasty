module Administration
  class MessagesController < AdminController
    include MessagesHelper

    def index
      @message = Message.new
      @messages = Message.all.order(created_at: :desc)
      @patients = Patient.all
    end

    def create
      @message = Message.create(messages_params)
      response = @message.send_custom_sms
      @message.update(sms_id: @message.id.to_s.rjust(7, 'D00000'), send_time: Time.now, status: :sent) if response == '0' || response == '11'
      nikita_response response
      redirect_to messages_path
    end

    def delivery
      patients_ids = params[:patient]
      message = params[:text]
      if patients_ids.include?('all')
        patients = Patient.all
        patients_ids = patients.pluck(:id)
        phones = patients.pluck(:phone)
      else
        phones = Patient.where(id: patients_ids).pluck(:phone)
      end
      @delivery = Delivery.create(patient_ids: patients_ids, message: message)
      if @delivery.save
        response = @delivery.send_delivery_sms phones
        nikita_response response
      end
      redirect_to messages_path
    end

    private

    def messages_params
      params.permit :phone, :text
    end
  end
end
